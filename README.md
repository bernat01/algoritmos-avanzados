# Practicas de la asignatura algoritmos avanzados

Resumen de los 6 algoritmos implementados dentro del repositorio en proyectos independientes.
Cada proyecto implemeta el siguiente algoritmo:

* aa_obl_1: Algoritmo de Dikjstra del camino minimo.
* aa_obl_2: Corrector automático de palabras con la distancia de Levenhstein
* aa_obl_3: algoritmo probabilístico.
* aa_opc_1: Backtracking.
* aa_opc_2:  Calculos distancia nube de puntos con distintos algoritmos. Visualización coste computacional de los algoritmos.
* aa_opc_3: algoritmo compresor de Huffman.