package Controller;

/**
 *
 * @author berna
 */
public class Aresta {
    private Vertex v1, v2;
    private double value;

    public Aresta(Vertex v1, Vertex v2, double value) {
        this.v1 = v1;
        this.v2 = v2;
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public Vertex getV1() {
        return v1;
    }

    public Vertex getV2() {
        return v2;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        Aresta a2 = (Aresta) obj;
        return (a2.getV1().getId() == this.v1.getId()) &&(a2.getV2().getId() == this.v2.getId());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + this.v1.getId();
        hash = 17 * hash + this.v2.getId();
        return hash;
    }

    @Override
    public String toString() {
        return "Aresta: src->"+this.v1.getId()+" dst->"+this.v2.getId(); //To change body of generated methods, choose Tools | Templates.
    }   
}
