package Controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Calculs {
    /**
     * Activar o desactivar missatges de execucio
     */
    private static final boolean DEBUG = false;
    
    /**
     * Mapejat dels nodees key => num. vertex, value => vertex
     */
    private HashMap<Integer, Vertex> nodes;

    public Calculs(HashMap<Integer, Vertex> nodes) {
        this.nodes = nodes;
    }
    
    /**
     * Retorna el camí mínim d'un node donat un vertex d'origen i destí
     * @param origin id del origen
     * @param desti id del destí
     * @return Llista amb la sequencia dels vertexs amb el camí mínim
     * @throws Exception 
     */
    public ArrayList<Vertex> shortestPath(int origin, int desti) throws Exception {
        Vertex vOr = nodes.get((Integer)origin);
        return shortestPath(vOr, nodes.get(desti));
    }
    
    /**
     * Retorna el camí mínim d'un node donat un vertex d'origen i destí
     * @param origin
     * @param desti
     * @return Llista amb la sequencia dels vertexs amb el camí mínim
     * @throws Exception 
     */
    public ArrayList<Vertex> shortestPath(Vertex origin, Vertex desti) throws Exception {
        final PriorityQueue<Vertex> pendents;
        final ArrayList<Vertex> permanents = new ArrayList<>();
        nodes.entrySet().stream().map((pair) -> (Vertex) pair.getValue()).forEachOrdered((v) -> {
            v.setCost(Double.MAX_VALUE);
        });
        origin.setCost(0);
        pendents = new PriorityQueue<Vertex>();
        pendents.offer(origin);
        Vertex current = null;    
        while (!pendents.isEmpty() && !desti.areEquals(current)) { 
            current = pendents.poll();
            permanents.add(current);
            updatePendents(current, pendents, permanents);
            this.printNodes();
        }
        this.printNodes();
        return retrievePath(origin, desti);
    }
    
   
    /**
     * Marcam els nodes successors del node v que estan a la llista de pendents,
     * els quals el seu accés a través de v es menor al que teniem registrat.
     * @param v Vertex al qual volem actualitzar els seus nodes successors
     * @param pendents 
     */
    private void updatePendents(Vertex v, PriorityQueue<Vertex> pendents, ArrayList<Vertex> permanents) {
        for (int i = 0; i < v.getNAristas(); i++) {          
            Vertex v1 = v.aristaTo(i);
            if (permanents.contains(v1)) {//si el successor ya esta marcado lo saltamos
                continue;
            }
            Aresta a = v.getArista(i);
            BigDecimal cost = BigDecimal.valueOf(v.getCost()).add(BigDecimal.valueOf(a.getValue()));
            if (cost.doubleValue() < v1.getCost()) {
                v1.setCost(cost.doubleValue());
                pendents.offer(v1);
            }
        }
    }

    private ArrayList<Vertex> retrievePath(Vertex origin, Vertex desti) throws Exception {
        ArrayList<Vertex> path = new ArrayList<>();
        Aresta prev = null;
        Vertex actual = desti;
        printNodes();
        while (actual.getId() != origin.getId()) {
            path.add(actual);            
            for (int i = 0; i < actual.getNAristas(); i++) {
                Aresta a = actual.getArista(i);
                if (a.equals(prev)){
                   continue;
                }
                
                Vertex candidat = actual.aristaTo(i);
                BigDecimal prevCost = BigDecimal.valueOf(actual.getCost()).subtract(BigDecimal.valueOf(a.getValue()));
                if (prevCost.equals(BigDecimal.valueOf(candidat.getCost()))) {
                   prev = a;
                   actual = candidat;
                   break;
                }
            }
            if (prev == null){//hem comprovat tots els succesors posibles
                return null;
            }
        }
        path.add(origin);
        return path;
    }
    
    /**
     * Funció de debug escriu per pantalla l'estat dels nodes en el
     * moment de la crida
     */
    private void printNodes(){
        if (! DEBUG){
            return;
        }
        System.out.println("content:");
        nodes.entrySet().forEach((pair) -> {
            Vertex v = (Vertex) pair.getValue();
            System.out.println("key: "+pair.getKey()+" value: "+pair.getValue()+ " size: ");
            System.out.println("NODE: "+v.getId()+" cost: "+v.getCost());
        });
    }

}
