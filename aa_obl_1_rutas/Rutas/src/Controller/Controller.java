package Controller;

import Model.Model;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que implementa tots els esdeveniments de la vista
 * 
 * @author bernat
 */
public class Controller implements ActionListener{
    /**
     * Instància del model amb la que interactua el controlador
     */
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            Calculs c;
            String accio = ae.getActionCommand();
            switch (accio) {
                case "compute":
                    c = buildGraph();
                    ArrayList<Vertex> results = c.shortestPath(model.getIdSrc(), model.getIdDst());
                    int [] path = new int[results.size()];
                    for(int i = 0; i< results.size(); i++){
                        Vertex v = results.get(i);
                        path[i]=v.getId();
                    }
                    model.setShortestPath(path);
                    break;
                    
                case "changeOrigin":
                    JComboBox cb_orig = (JComboBox) ae.getSource();//get the combobox
                    int posSrc =  cb_orig.getSelectedIndex();
                    model.setSrcCB(posSrc);
                    break;
                    
                case "changeDest":
                    JComboBox cb_dest = (JComboBox) ae.getSource();//get the combobox
                    int posDst = cb_dest.getSelectedIndex();
                    model.setDstCB(posDst);
                    break;
        
                default:
                    throw new badSelection();
            }
            
        } catch (badSelection ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    /**
     * Construeix el graf a partir de les dades del model
     * @return un objecte per fer els càlculs
     */
    private Calculs buildGraph(){
        int nRoads = model.numRoads();
        int nTowns = model.numTowns();
        Aresta[] roads = new Aresta[nRoads];
        HashMap<Integer, Vertex> towns = new HashMap<Integer, Vertex>();
        for(int i = 0;i < nTowns; i++){
            int town = model.getTown(i);
            towns.put(town, new Vertex(town));
        }
        
        for(int i = 0;i < nRoads; i++){
            int[] road = model.getRoad(i);
            roads[i]= new Aresta(towns.get(road[0]), towns.get(road[1]), model.getRoadDist(i));
            towns.get(road[0]).ponArista(roads[i]);
            towns.get(road[1]).ponArista(roads[i]);
        }
        
        return new Calculs(towns);
    }
    
    private class badSelection extends Exception{}
    
}
