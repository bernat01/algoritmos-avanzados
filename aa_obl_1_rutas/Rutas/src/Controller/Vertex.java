package Controller;

import java.util.ArrayList;

/**
 *
 * @author berna
 */
public class Vertex implements Comparable<Vertex>{
    private int id;
    private ArrayList <Aresta> salientes;
    private double cost;
    
    
    public Vertex(int id) {
        this.id = id;
        salientes = new ArrayList <Aresta> ();
        this.cost = Double.MAX_VALUE;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
    
    protected void ponArista(Aresta a) {
        salientes.add(a);
    }
    
    protected int getNAristas() {
        return salientes.size();
    }

    protected Aresta getArista(int i) {
        return salientes.get(i);
    }
    
    protected Vertex aristaTo(int nAresta){
         Aresta a = salientes.get(nAresta);
        return (a.getV1() == this) ? a.getV2() : a.getV1();
            
    }

    public int getId() {
        return id;
    }

    public double getCost() {
        return cost;
    }

    @Override
    public int compareTo(Vertex t) {
        if (this.getCost() < t.getCost())
            return -1;
        else if (this.getCost() > t.getCost())
            return 1;
        return 0;
    }

    public boolean areEquals(Vertex v){
        if ( v == null){
            return false;
        }
        return this.getId() == v.getId();
    }
    
   
}
