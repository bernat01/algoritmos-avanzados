package Model;

import java.util.ArrayList;
import java.util.HashMap;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Classe que conté els events per processar el fitxer XML mitjançant un SAXParser
 * @author Bernat Galmés Rubert
 */
public class Graph extends DefaultHandler {

    /**
     * ESTRUCTURES UTILITZADES PER PARSEJAR EL FITXER
     */
   
    public class Tag {
        final static String MALLORCA = "Mallorca";
        final static String POBLES = "pobles";
        final static String POB = "pob";
        final static String CARRETERES = "carreteres";
        final static String CARRETERA = "carr";
    }

    private enum t_data {
        POBLES, CARRETERES, NONE
    };

   
    private t_data parsing = t_data.NONE;

    private HashMap<Integer, Town> nodos;
    private ArrayList<Road> aristas;

    private boolean enGrafo = false;
    private Attributes currentTown;
    private Attributes currentRoad;
    
    public Graph() {
        super();
        nodos = new HashMap<Integer, Town>();
        aristas = new ArrayList<Road>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase(Tag.MALLORCA)) {
            enGrafo = true;

        } else if (qName.equalsIgnoreCase(Tag.POBLES)) {
            this.parsing = t_data.POBLES;

        } else if (qName.equalsIgnoreCase(Tag.CARRETERES)) {
            this.parsing = t_data.CARRETERES;

        } else if (qName.equalsIgnoreCase(Tag.POB)) {
            this.currentTown = attributes;

        } else if (qName.equalsIgnoreCase(Tag.CARRETERA)) {
            this.currentRoad = attributes;
        }
    }

    @Override
    public void endElement(String uri, String localName,
            String qName) throws SAXException {
        if (qName.equalsIgnoreCase(Tag.MALLORCA)) {
            enGrafo = false;

        } else if (qName.equalsIgnoreCase(Tag.POBLES)) {
            this.parsing = t_data.NONE;

        } else if (qName.equalsIgnoreCase(Tag.CARRETERES)) {
            this.parsing = t_data.NONE;

        } else if (qName.equalsIgnoreCase(Tag.POB)) {
            this.currentTown = null;

        } else if (qName.equalsIgnoreCase(Tag.CARRETERA)) {
            this.currentRoad = null;
        }
    }

    @Override
    public void characters(char ch[], int start, int length)
            throws SAXException {
        Town p;
        String value = new String(ch, start, length).trim();
        switch (parsing) {
            case POBLES:
                if (currentTown == null) {
                    break;
                }
                /* 
                * Suposam que l'xml esta ben format quan parsejam pobles nomes 
                * tenim tag pobles i nomes hi ha contingut dins aquest tag
                * IDEM PER CARRETERES
                 */
                String location[] = value.split(":");
                int idP = Integer.parseInt(this.currentTown.getValue("id"));
                String name = this.currentTown.getValue("name");
                if (location.length != 2) {
                    System.err.println("bad xml: " + location.length + " town: " + name + "value: " + value);
                    break;
                }
                p = new Town(idP, name,
                        Integer.parseInt(location[0]), Integer.parseInt(location[1]));
                nodos.put(idP, p);
                break;

            case CARRETERES:
                if (currentRoad == null) {
                    break;
                }
                String s[] = value.split(":");
                Town p1 = nodos.get(Integer.parseInt(s[0]));
                Town p2 = nodos.get(Integer.parseInt(s[1]));
                Road c = new Road(Double.parseDouble(s[2]), p1, p2);
                p1.ponArista(c);
                p2.ponArista(c);
                aristas.add(c);
                break;

            default:
                break;
        }
    }

    public HashMap<Integer, Town> getNodos() {
        return nodos;
    }

    public Road[] getAristas() {
        Road[] carreteras = new Road[aristas.size()];
        return aristas.toArray(carreteras);
    }
}
