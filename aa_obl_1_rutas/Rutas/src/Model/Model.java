package Model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import rutas.Errores;

/**
 * Interfície del model Classe per accedir a les dades del programa
 *
 * @author Bernat Galmés
 */
public class Model extends Observable {

    /**
     * Dades vista
     */
    private int w = 800;
    private int h = 800;
    
    //mapa dels ids dels pobles amb els indexos del combobox de la vista
    private int[] map_towns;

    /**
     * Dades del problema
     */
    private HashMap<Integer, Town> pueblos;
    private Road[] carreteras;

    /**
     * Dades de la solució
     */
    private int[] shortestPath;
    private int idSrc = 1;
    private int idDst = 1;
    
    /**
     * Rutes fitxers
     */
    final String FILE_DATA = "src/data/data.xml";
    final String IMAGE_MAP = "src/data/Mapa.jpg";
    
    public Model() {
        Graph g = getGrafo();
        this.pueblos = g.getNodos();
        this.carreteras = g.getAristas();
    }

    /**
     * Construeix un objecte Graph a partir del fitxer XML
     * @return 
     */
    private Graph getGrafo() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        Graph g = new Graph();
        try {
            InputStream xmlInput
                    = new FileInputStream(FILE_DATA);
            SAXParser saxParser = factory.newSAXParser();

            saxParser.parse(xmlInput, g);

        } catch (Exception e) {
            Errores.informaError(e);
        }
        return g;
    }

    
    /**
     * FUNCIONS PER OBTENIR LES DADES DEL MODEL 
     */    
    
    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public int[] townPosition(int nTown) {
        return new int[]{this.pueblos.get(this.map_towns[nTown]).getX(), this.pueblos.get(this.map_towns[nTown]).getY()};
    }

    public String townName(int nTown) {
        return this.pueblos.get(this.map_towns[nTown]).getEtiqueta();
    }

    public int[] roadOrigin(int nRoad) {
        Town pOrig = this.carreteras[nRoad].getP1();
        return new int[]{pOrig.getX(), pOrig.getY()};
    }

    public int[] roadDesti(int nRoad) {
        Town pOrig = this.carreteras[nRoad].getP2();
        return new int[]{pOrig.getX(), pOrig.getY()};
    }

    public int roadOriginID(int nRoad) {
        return this.carreteras[nRoad].getP1().getId();
    }

    public int roadDestiID(int nRoad) {
        return this.carreteras[nRoad].getP2().getId();
    }

    public int[] getRoad(int nRoad) {
        Road c = carreteras[nRoad];
        return new int[]{c.getP1().getId(), c.getP2().getId()};
    }

    public int getTown(int nTown) {
        return this.pueblos.get(this.map_towns[nTown]).getId();
    }

    public double getRoadDist(int nRoad) {
        return this.carreteras[nRoad].getValor();
    }
    
    public String[] dataComboBox() {
        String[] Names = new String[this.pueblos.size()];
        this.map_towns = new int[this.pueblos.size()];
        int i = 0;
        
        for(Town t : this.pueblos.values()){
            Names[i] = t.getEtiqueta();
            System.out.println(Names[i]);
            this.map_towns[i] = t.getId();
            i++;
        }  
         
        return Names;
    }

    public int numTowns() {
        return this.pueblos.size();
    }

    public int numRoads() {
        return this.carreteras.length;
    }

    public void setShortestPath(int[] shortestPath) {
        this.shortestPath = shortestPath;

        this.setChanged();
        this.notifyObservers();
    }

    public int[] getShortestPath() {
        return shortestPath;
    }

    public int getIdSrc() {
        return idSrc;
    }

    public int getIdDst() {
        return idDst;
    }

    public void setSrcCB(int posTown) {
        this.idSrc = this.map_towns[posTown];
    }

    public void setDstCB(int posTown) {
        this.idDst = this.map_towns[posTown];
    }

    public ImageIcon getIMAGE_MAP() {
        try {
            return new ImageIcon(ImageIO.read(new File(this.IMAGE_MAP)));
        } catch (IOException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
