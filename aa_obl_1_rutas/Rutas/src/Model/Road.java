package Model;

/**
 *
 * @author mascport
 */
public class Road {
    private double valor;//coste
    private Town p1, p2;//pueblos que une
    
    public Road(double v, Town p1, Town p2) {
        valor = v;
        this.p1=p1;
        this.p2=p2;
    }
    
    public double getValor() {
        return valor;
    }

    public Town getP1() {
        return p1;
    }

    public Town getP2() {
        return p2;
    }
    
}
