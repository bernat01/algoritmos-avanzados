package Model;

import java.util.ArrayList;

public class Town {
    
    private int id;
    private String etiqueta;
    private ArrayList <Road> salientes;
    private int X;
    private int Y;
    

    public Town(int id, String et, int x, int y) {
        this.id = id;
        etiqueta = et;
        X = x;
        Y = y;
        salientes = new ArrayList <Road> ();
    }
    
    protected void ponArista(Road a) {
        salientes.add(a);
    }
    
    protected String getEtiqueta() {
        return etiqueta;
    }
    
    protected int getX() {
        return X;
    }
    
    protected int getY() {
        return Y;
    }

    protected int getNAristas() {
        return salientes.size();
    }

    protected Road getArista(int i) {
        return salientes.get(i);
    }
   
    public int getId() {
        return id;
    }

    
    @Override
    public String toString() {
        return this.etiqueta; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
