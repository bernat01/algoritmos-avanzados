/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.font.TextAttribute;
import java.text.AttributedString;

/**
 *
 * @author berna
 */
public class Graphics2Dutils {
    private Graphics2D g2d;

    public Graphics2Dutils(Graphics2D g2d) {
        this.g2d = g2d;
    }
    
    public void drawString(String text, int x, int y, Color colBackground) {
        AttributedString as1 = new AttributedString(text);
        as1.addAttribute(TextAttribute.BACKGROUND, colBackground);
        g2d.drawString(as1.getIterator(), x, y);
    }
}
