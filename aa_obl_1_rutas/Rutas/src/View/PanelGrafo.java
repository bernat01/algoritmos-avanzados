package View;

import Model.Model;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.JPanel;

/**
 *
 * @author mascport
 */
public class PanelGrafo extends JPanel {

    private Model model;
    private final int paso = 80;
    private final int tam = 30;
    private int ancho;
    private int alto;
    private int path[];
    private Image background;
    private Font textFont;
    
    public PanelGrafo(Model model) {
        this.model = model;
        this.background = model.getIMAGE_MAP().getImage();
        ancho = model.getW();
        alto = model.getH();
        Dimension size = new Dimension(ancho, alto);
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
        textFont = new Font("Verdana", Font.BOLD, 16);
        
        emptyPath();
    }
    
    public PanelGrafo(Image img) {
        this.background = img;
      }

    @Override
    public void repaint() {
        Graphics g = this.getGraphics();
        if (g != null) {
            this.paint(g);
        }
    }

    @Override
    public void paint(Graphics g) {
        g.setColor(new Color(250, 250, 250));
        g.setFont(textFont);
        
        g.fillRect(0, 0, ancho, alto);
        g.drawRect(0, 0, ancho, alto);
        g.drawImage(this.background, 0, 0, ancho, alto, null);
        pintarGrafo(g);
    }

    private void drawNodes(Graphics2D g){
        //pintoNodos
        int nnodos = this.model.numTowns();
        int maxw = ancho;
        int maxh = alto;
        g.setStroke(new BasicStroke(4));
        for (int i = 0; i < nnodos; i++) {
            int[] nodePos = model.townPosition(i);
            int X = nodePos[0];
            int Y = nodePos[1];
            g.setColor(new Color(0, 0, 0));
            g.drawOval((X + 1) * paso, (Y + 1) * paso, tam, tam);
            g.setColor(new Color(255, 255, 255));
            g.fillOval((X + 1) * paso, (Y + 1) * paso, tam, tam);
            
            if (((X + 1) * paso + tam) > maxw) {
                maxw = (X + 1) * paso + paso;
            }
            if (((Y + 1) * paso + tam) > maxh) {
                maxh = (Y + 1) * paso + paso;
            }
        }
        if ((maxw > ancho) || (maxh > alto)) {
            if (maxw > ancho) {
                ancho = maxw;
            }
            if (maxw > alto) {
                alto = maxh;
            }
            this.setPreferredSize(new Dimension(ancho, alto));
            this.revalidate();
            this.getParent().getParent().repaint();
        }
    }
      private void drawCityLabels(Graphics2D g){
        int nnodos = this.model.numTowns();
        for (int i = 0; i < nnodos; i++) {
            int[] nodePos = model.townPosition(i);
            int X = nodePos[0];
            int Y = nodePos[1];
            
            g.setColor(new Color(0, 0, 0));
            String s = model.townName(i);
            (new Graphics2Dutils(g)).drawString(s, (X + 1) * paso -3, (Y + 1) * paso
                    + (int) (tam / 1.8), Color.LIGHT_GRAY);
        }
    }
      
    private void drawDistances(Graphics2D g){
        //pintoAristas
        g.setStroke(new BasicStroke(5));//amplitud aristas
        g.setColor(new Color(0, 0, 0));
        int nAristas = model.numRoads();
        for (int j = 0; j < nAristas; j++) {            
            int[] origPos = model.roadOrigin(j);
            int[] destPos = model.roadDesti(j);
            int X = origPos[0];
            int Y = origPos[1];
            int X1 = destPos[0];
            int Y1 = destPos[1];
            int a = (X1 + 1) * paso;
            int b = (Y1 + 1) * paso;
            int x = (X + 1) * paso;
            int y = (Y + 1) * paso;
            int aux = tam / 2;
            
            double d = model.getRoadDist(j);
            String s = Double.toString(d);
            int posLabelX =((x+aux)+(a+aux))/2- 15;
            int posLabelY = ((y + aux)+(b + aux))/2;
           (new Graphics2Dutils(g)).drawString(s, posLabelX, posLabelY, Color.WHITE);
        }
    }
   
    private void drawAristas(Graphics2D g){
        //pintoAristas
        g.setStroke(new BasicStroke(5));//amplitud aristas
        g.setColor(new Color(0, 0, 255));
        int nAristas = model.numRoads();
        for (int j = 0; j < nAristas; j++) {            
            int[] origPos = model.roadOrigin(j);
            int[] destPos = model.roadDesti(j);
            int X = origPos[0];
            int Y = origPos[1];
            int X1 = destPos[0];
            int Y1 = destPos[1];
            int a = (X1 + 1) * paso;
            int b = (Y1 + 1) * paso;
            int x = (X + 1) * paso;
            int y = (Y + 1) * paso;
            int aux = tam / 2;
            int idOrig = model.roadOriginID(j);
            int idDesti = model.roadDestiID(j);
            
            if(path[idOrig]==idDesti || path[idDesti]==idOrig){
                g.setColor(new Color(127, 0, 0));
                
            }else{
                g.setColor(new Color(0, 0, 127));
            }
            g.drawLine((x + aux), y + aux, a + aux, b + aux);
        }
    }

    private void pintarGrafo(Graphics gr) {
        Graphics2D g = (Graphics2D) gr;
        this.drawAristas(g);
        this.drawNodes(g);            
        this.drawCityLabels(g);
        this.drawDistances(g);
    }
    
    private void emptyPath(){
        path = new int[model.numTowns()+1];///LA POSICIÓ 0 MAI S'UTILITZA
       for(int i=0;i<path.length;i++){
            path[i]= -1;
        }
    }
    public void printPath(int[] newPath){
        emptyPath();
        
        for(int i=0;i<newPath.length-1;i++){
            int v = newPath[i];
            path[v]=newPath[i+1];
        }
        this.repaint();
    }
}
