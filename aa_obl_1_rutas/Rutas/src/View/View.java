/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Model;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;

/**
 *
 * @author berna
 */
public class View implements Observer{
    
    private Model model;
    private final String WIN_NAME = "Mallorca";
    
    private Window gui;
    
    public View(Model model) {
        this.model = model;
        
        gui = new Window(WIN_NAME, this.model);
        gui.getCb_origen().setSelectedIndex(model.getIdSrc()-1);
        gui.getCb_origen().setSelectedIndex(model.getIdDst()-1);
    }

     public void addController(ActionListener controller) {
        gui.getBotonCalculo().setActionCommand("compute");
        gui.getBotonCalculo().addActionListener(controller);
        
        gui.getCb_origen().setActionCommand("changeOrigin");
        gui.getCb_origen().addActionListener(controller);
    
        gui.getCb_destino().setActionCommand("changeDest");
        gui.getCb_destino().addActionListener(controller);
     }
     
    public void mostrar() {
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.pack();
        gui.setVisible(true);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        int path[] = model.getShortestPath();
        gui.printPath(path);
    }
    
}
