package View;

import Model.Model;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;

public class Window extends JFrame{
    public static int w, h;//window size
    
    private JComboBox cb_origen, cb_destino;
    private JButton botonCalculo;
    private PanelGrafo imprimir;
    private JProgressBar porc;
    
    private JScrollPane sp;
    private Model model;

    public Window(String s, Model model) {
        super(s);
        this.model = model;
        this.getContentPane().setLayout(new BorderLayout());
        this.add(entrada(), BorderLayout.NORTH);
        this.add(centro(), BorderLayout.CENTER);
        porc = new JProgressBar();
        porc.setValue(0);
        this.add(porc, BorderLayout.SOUTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    

    private JPanel entrada() {
        JPanel pan = new JPanel();
        cb_origen = new JComboBox(model.dataComboBox());
        cb_destino = new JComboBox(model.dataComboBox());
        
        pan.setLayout(new FlowLayout());
        pan.add(new JLabel("Ciudad de origen:"));
        pan.add(cb_origen);
        pan.add(new JLabel("Ciudad de destino:"));
        pan.add(cb_destino);
        pan.add(new JLabel("         "));
        botonCalculo = new JButton("Computar");
        pan.add(botonCalculo);
        return pan;
    }

    private JPanel centro() {
        JPanel pan = new JPanel();
        imprimir = new PanelGrafo(model);
        sp = new JScrollPane(imprimir);
        sp.setPreferredSize(new Dimension(model.getW(), model.getH()));
        pan.add(sp);
        return pan;
    }

    public void printPath(int [] path){
        imprimir.printPath(path);        
    }
    
    public JButton getBotonCalculo() {
        return botonCalculo;
    }

    public JComboBox getCb_origen() {
        return cb_origen;
    }

    public JComboBox getCb_destino() {
        return cb_destino;
    }    
}
