/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rutas;

import Controller.Controller;
import Model.Model;
import View.View;

/**
 * Efectuar una aplicación siguiendo el patrón de diseño MVC 
 * (se valorará el uso de las clases e interfaces de Observer) que 
 * dado un grafo proporcionado por un fichero de texto en XML o JSON con la información de los pueblos y principales carreteras de Mallorca, 
 * sea capaz de hallar el camino mínimo entre dos vértices cualesquiera de dicho grafo.
 * El programa debe de presentar una IGU capáz de preguntar por los dos Pueblos(origen y destino) 
 * y debe de poder visualizar el resultado.
 * 
 * Para poder efectuar correctamente el patrón de diseño MVC de nuevo deberemos disponer de un lenguage que permita la POO o que disponga del uso de TAD.
 * 
 * Como tareas adicionales se podrá presentar la solución en un mapa (las carreteras pueden ser rectas y se podrán colocar en la ruta más corta puntos intermedios obligatorios).
 * @author berna
 */
public class Rutas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Model model = new Model();
        View view = new View(model);
        Controller  controller= new Controller(model);
        
        view.addController(controller);
        model.addObserver(view);
        
        view.mostrar();
        
    }
    
}
