package Controller;

import Model.Model;
import View.View;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;

/**
 * Classe encarregada de la comunicació del controllador amb els altres dos mòduls
 * @author bernat
 */
public class Controller implements ActionListener{
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        
    }
    
    public static void Config(){
        Correction.setConfig(Model.getMAX_OPC(), Model.getMIN_DIST(), Model.getDEBUG());
        Levenshtein.setMAX_DIST(Model.getMAX_OPC());
    }
    /*
    * Events de la vista
    */
    @Override
    public void actionPerformed(ActionEvent ae) {
        try {           
            String accio = ae.getActionCommand();
            switch (accio) {
                case "correct":
                    view.updateModel();
                    new Process(model, model.getText());//iniciam el proces del corrector
                    break;
                case "changeDict":
                    JComboBox cb_dict = (JComboBox) ae.getSource();//get the combobox
                    int posDict =  cb_dict.getSelectedIndex();
                    model.setActiveDict(posDict);
                    break;
                default:
                    throw new badSelection();
            }
            
        } catch (badSelection ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
  
    
    private class badSelection extends Exception{}
    
}
