package Controller;

import java.util.ArrayList;

/**
 * Classe que efectua la correcció d'un text
 * @author Bernat Galmés Rubert
 */
public class Correction {

    //problem data with default values, used values are in model
    private static int MAX_OPC = 5;
    private static int MIN_DIST = 3;

    private static boolean DEBUG;

    private char[][] palabras;
    private ArrayList<Dictionary> dictionaries;

    private int[] distances;
    private LimitedPriorityQueue<Word>[] SimilarWords;

    public Correction(String text, String delimRegex){      
        String[] pals = text.split(delimRegex);//
        this.palabras = new char[pals.length][];
        for (int i = 0; i < pals.length; i++) {
            pals[i] = pals[i].toLowerCase();//a minúscules 
            if(DEBUG){
                System.out.println("in word: " + pals[i]);

            }
            this.palabras[i] = pals[i].toCharArray();
        }

        dictionaries = new ArrayList<Dictionary>();
        distances = new int[pals.length];
        SimilarWords = new LimitedPriorityQueue[pals.length];
        for (int i = 0; i < pals.length; i++) {
            this.SimilarWords[i] = new LimitedPriorityQueue(MAX_OPC);
        }
    }
    
    /**
     * Configura els objectes de Calculs segons la configuració del sistema
     * @param max_opc
     * @param min_dist
     * @param debug 
     */
    public static void setConfig(int max_opc, int min_dist, boolean debug){
        MAX_OPC = max_opc;
        MIN_DIST = min_dist;
        DEBUG = debug;
    }
    

    public void AddDictionary(String filename) {
        dictionaries.add(new Dictionary(filename));
    }

    public void correction() {
        //Calculam la distancia de levenstein per totes les paraules
        for (int i = 0; i < palabras.length; i++) {
            char[] word = this.palabras[i];
            checkWord(word, i);
        }        
    }
   
    public String[] getSimilarWords() {
        String[] res = new String[this.SimilarWords.length];
        
        int i = 0;
        for (LimitedPriorityQueue<Word> ws : this.SimilarWords) {
            res[i] = ws.toString();
            i++;
        }
        return res;
    }

    public int[] getDistances() {
        return distances;
    }

    private void checkWord(char[] word, int nw) {
        Word wMin = new Word();
        wMin.d = Integer.MAX_VALUE;
        Levenshtein lev = new Levenshtein(word);
        for (Dictionary dict : dictionaries) {
            dict.reset();
            char[] dWord = dict.getWord();
            while (dWord != null) {
                int dist = lev.distance(dWord);

                if (dist == 0) {//paraula al dictionari
                    distances[nw] = 0;
                    return;
                }
                if (dist < MIN_DIST) {
                    SimilarWords[nw].offer(new Word(dWord, dist));
                }

                if (dist < wMin.d) {
                    wMin.d = dist;
                    wMin.s = dWord;
                }
                dWord = dict.getWord();
            }
        }
        distances[nw] = wMin.d;
    }

  

    private class Word implements Comparable<Word>{
        char[] s;
        int d;

        public Word(char[] s, int d) {
            this.s = s;
            this.d = d;
        }

        public Word() {}

        @Override
        public String toString() {
            return String.valueOf(this.s) + " "+this.d;
        }

        @Override
        public int compareTo(Word t) {
            return (new Integer(this.d)).compareTo(t.d);
        }
    }
}
