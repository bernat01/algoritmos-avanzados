package Controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe que monta un diccionari a partir d'un fitxer
 * Serveix per iterar sequencialment sobre les paraules del diccionari
 * @author Bernat Galmés Rubert
 */
public class Dictionary {
    
    private FileInputStream fr;
    private BufferedReader br;
    private InputStreamReader isr = null;

    /**
     * 
     * @param filePath path fichero con una palabra en cada linea
     */
    public Dictionary(String filePath) {
        try {
            this.fr = new FileInputStream(filePath);
            this.isr = new InputStreamReader(this.fr);
            this.br = new BufferedReader(this.isr);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Dictionary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
 
    /**
     * Devuelve una palabra distinta del diccionario cada vez.
     * cuando no quedan palabras devuelve null
     * @return 
     */
    public char[] getWord(){
        try {
            String line= br.readLine();
            if (line != null) {
                return line.toCharArray();
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return null;
    }
 
    /**
     * Reinicia la lectura del diccionari
     */
    public void reset(){
        try {
            this.fr.getChannel().position(0);
            this.br = new BufferedReader(new InputStreamReader(this.fr));
        } catch (IOException ex) {
            Logger.getLogger(Dictionary.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     *
     * @throws Throwable
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize(); //To change body of generated methods, choose Tools | Templates.
        if (br != null) {
            br.close();
        }
    }
    
    
}
