package Controller;

/**
 *
 * @author bernat
 */
public class Levenshtein {
    private static int MAX_DIST;
    
    private char[] word;

    public Levenshtein(char[] word) {
        this.word = word;
    }
    public Levenshtein(String word) {
        this.word = word.toCharArray();
    }
    
    public int distance(char[] word){
        int [][]distance = new int[this.word.length+1][word.length+1];

        for(int i=0;i<=this.word.length;i++){
                distance[i][0]=i;
        }
        for(int j=0;j<=word.length;j++){
                distance[0][j]=j;
        }
        for(int i=1;i<=this.word.length;i++){
            for(int j=1;j<=word.length;j++){ 
                distance[i][j] = minimum(distance[i-1][j]+1,//insert
                                        distance[i][j-1]+1,//delete
                                        distance[i-1][j-1] + ((this.word[i-1]==word[j-1])?0:1));//update
                if (distance[i][j] >= MAX_DIST){
                    return Integer.MAX_VALUE;
                }
            }
        }
        return distance[this.word.length][word.length];
    }
     
    /**
     * Retorna el valor minim dels tres pasats per paràmetre
     * @param a
     * @param b
     * @param c
     * @return 
     */
    private static int minimum(int a, int b, int c) {
        if(a<=b && a<=c){
            return a;
        } 
        if(b<=a && b<=c){
            return b;
        }
        return c;
    }  

    public static void setMAX_DIST(int MAX_DIST) {
        Levenshtein.MAX_DIST = MAX_DIST;
    }
}
