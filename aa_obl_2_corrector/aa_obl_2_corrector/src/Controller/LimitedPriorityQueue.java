package Controller;

import java.util.PriorityQueue;

/**
 * Coa de prioritat amb un nombre finit d'elements
 * @author berna
 */
public class LimitedPriorityQueue<E extends Comparable> extends PriorityQueue<E>{

    private int max_size;
    
    /**
     * Constructor
     * @param max Numero máximo de elementos de la cola
     */
    public LimitedPriorityQueue(int max) {
        super(max);
        this.max_size = max;
    }

    /**
     * Añade un elemento a la cola.
     * Añade elementos a la cola hasta el limite que tiene fijado. Si la cola
     * ya tiene el numero màximo de elementos i se intenta añadir un elemento más grande que el 
     * mayor también lo añade.
     * @param e Objeto que se quiere añadir
     * @return exito o fracaso de la insercion
     */
    @Override
    public boolean offer(E e) {
        if (super.size() != 0 && e.compareTo(this.peek()) < 0){
            return super.offer(e); 
        }
        
        if (super.size() < max_size){//TODO: treure el element amb més distància
            return super.offer(e); //To change body of generated methods, choose Tools | Templates.
        }
        return false;
    }    
}
