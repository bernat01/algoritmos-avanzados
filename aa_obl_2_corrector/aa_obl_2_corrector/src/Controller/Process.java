package Controller;

import Model.Model;

/**
 *
 * @author berna
 */
class Process implements Runnable {

    private String text;
    private Correction c;
    private Model model;

    public Process(Model model, String text) {
        this.text = text;
        this.model = model;
        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        System.out.println(text);
        this.c = new Correction(text, model.getWordDelimiterRegex());
        /*for (String pathdict: model.getDictionaries()){
              c.AddDictionary(pathdict);
          }*/
        c.AddDictionary(model.getActiveDict());
        c.correction();
        model.setResults(c.getDistances(), c.getSimilarWords());
    }
}
