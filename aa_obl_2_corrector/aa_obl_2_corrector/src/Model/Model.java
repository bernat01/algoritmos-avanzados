package Model;

import java.awt.Color;
import java.io.File;
import java.util.Observable;
import javax.swing.text.Style;

/**
 *
 * @author berna
 */
public class Model extends Observable {

    /**
     * Contingut estatic
     */
    //window configuration
    private static final String WIN_NAME = "Corrector ortogràfic";
    private static final int W = 600;
    private static final int H = 500;
    
    
    private static final String tree_results_rootName = "Paraules errònees";
    
    //Solution data
    private static final int MAX_OPC = 5;//nombre d'alternatives a mostrar a la paraula errada
    private static final int MIN_DIST = 3;//distancia a partir de la qual descartar les paraules

    private static final boolean DEBUG = false;//activa el debug del sistema
    
    
    /**
     * View data. Data about text visualization
     */
    private final int text_correctSize = 16;
    private final int text_errorSize = 16;

    private final Color text_correctColor = Color.BLACK;
    private final Color text_errorColor = Color.RED;

    /**
     * PROGRAM CONSTANTS
     */
    private final String PATH_DICTIONARIES = "src/words/";
    private final char[] wordDelimiters = {'.', ',', '?', '¿', '¡', '!', ' ', '\n'};//delimitadors de paraules        

    /**
     * Contingut específic de la instància
     */
    
    /**
     * Data used by the alghorithm
     */
    private String[] dictionaries;//dictionaries used
    int nActiveDict = 0;//current selected dictionary position
    private String text;//text introduced

    //resultats
    private int[] res_distances;// list of similar words distances
    private String[] res_similar;//list of similar word

    

    public Model() throws Exception {
        inspectDictionaries();
    }

    /**
     * Inspect the path of dictionaries to build the dictionaries array
     *
     * @throws Exception
     */
    private void inspectDictionaries() throws Exception {
        File dir = new File(PATH_DICTIONARIES);
        File[] directoryListing = dir.listFiles();
        if (directoryListing == null) {
            throw new Exception("no dictionaries found");
        }
        this.dictionaries = new String[directoryListing.length];
        for (int i = 0; i < dictionaries.length; i++) {
            dictionaries[i] = directoryListing[i].getPath();
            System.out.println(dictionaries[i]);
        }
    }

    public String[] getDictionariesNames() throws Exception {
        File dir = new File(PATH_DICTIONARIES);
        File[] directoryListing = dir.listFiles();
        if (directoryListing == null) {
            throw new Exception("no dictionaries found");
        }
        String[] names = new String[directoryListing.length];
        for (int i = 0; i < dictionaries.length; i++) {
            names[i] = directoryListing[i].getName();
        }
        return names;
    }

    public static int getW() {
        return W;
    }

    public static int getH() {
        return H;
    }

    public static String getWIN_NAME() {
        return WIN_NAME;
    }

    public static String getTree_results_rootName() {
        return tree_results_rootName;
    }

    public String getWordDelimiterRegex() {
        String delimRegex = "[";
        for (char delim : wordDelimiters) {
            delimRegex += "[" + delim + "]*|";

        }
        delimRegex += "]+";
        return delimRegex;
    }

    public String[] getDictionaries() {
        return this.dictionaries;
    }

    public void setActiveDict(int n) {
        this.nActiveDict = n;
    }

    public String getActiveDict() {
        return dictionaries[nActiveDict];
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setResults(int[] res_distances, String[] res_similar) {
        this.res_distances = res_distances;
        this.res_similar = res_similar;

        this.setChanged();
        this.notifyObservers();
    }

    public int[] getRes_distances() {
        return res_distances;
    }

    public String[] getRes_similar() {
        return res_similar;
    }

    public static int getMAX_OPC() {
        return MAX_OPC;
    }

    public static int getMIN_DIST() {
        return MIN_DIST;
    }

    public static boolean getDEBUG() {
        return DEBUG;
    }

    public Style getStyleCorrectText() {
        return (new TextStyle(this.text_correctColor, this.text_correctSize)).getStyle();
    }

    public Style getStyleErrorText() {
        return (new TextStyle(this.text_errorColor, this.text_errorSize)).getStyle();
    }
}
