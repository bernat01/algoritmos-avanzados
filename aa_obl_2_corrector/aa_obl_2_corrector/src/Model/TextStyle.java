package Model;

import java.awt.Color;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * Classe per obtenir l'estil d'un text
 * @author berna
 */
public class TextStyle {

    private Style style;
    
    public TextStyle(Color textColor, int fontSize) {
        StyleContext corr = new StyleContext();
        style = corr.addStyle("test", null);
        StyleConstants.setForeground(style, textColor);
        StyleConstants.setFontSize(style, fontSize);
    }

    public Style getStyle() {
        return style;
    }
}
