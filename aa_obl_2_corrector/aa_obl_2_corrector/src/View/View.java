package View;

import Model.Model;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.text.DefaultStyledDocument;

/**
 * Classe que agrupa tots els elements de la vista
 * @author berna
 */
public class View implements Observer{
    
    private Model model;
       
    private Window gui;
    private textCorrectionPane textPane;
    
    public View(Model model) {
        this.model = model;
        textPane = new textCorrectionPane(model.getStyleCorrectText(), model.getStyleErrorText(), new DefaultStyledDocument());     
        try {
            gui = new Window(this.model.getWIN_NAME(), textPane, this.model.getDictionariesNames());
        } catch (Exception ex) {
            Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void Config(){
        Window.setConfig(Model.getW(), Model.getH(), Model.getTree_results_rootName());
    }

     public void addController(ActionListener controller) {
        gui.getBtn_corection().setActionCommand("correct");
        gui.getBtn_corection().addActionListener(controller);
        
        gui.getSelect_dictionary().setActionCommand("changeDict");
        gui.getSelect_dictionary().addActionListener(controller);
        
     }
     
    public void mostrar() {
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.pack();
        gui.setVisible(true);
    }
    
    public void updateModel(){
        this.model.setText(gui.getText());
    }
    
    @Override
    public void update(Observable o, Object arg) {
        int[] dists  = model.getRes_distances();
        String[] words = model.getRes_similar();
        
        try {
            textPane.updateText(dists, words, model.getWordDelimiterRegex());
            gui.updateTree(dists, words, model.getWordDelimiterRegex());
            if(Model.getDEBUG()){
                for(int i =0;i<dists.length;i++){
                    System.out.println("distance: "+ dists[i]+ " proposed: "+words[i]);
                }
            }
        } catch (Exception ex) {
           Logger.getLogger(View.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
