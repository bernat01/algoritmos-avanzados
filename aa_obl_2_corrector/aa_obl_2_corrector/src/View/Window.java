package View;

import Custom.DynamicTree;
import java.awt.Dimension;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Classe de la finestra que es mostra per pantalla
 * @author berna
 */
public class Window  extends javax.swing.JFrame {
    /**
     * Static content, set this values with setConfig method before instance any window
     */
    private static int W;
    private static int H;  
    private static String RootName;
    
    private JComboBox select_dictionary;
    private String[] list_dictionaries;
    
    public Window(String s, textCorrectionPane textPane, String[] list_dictionaries) {
        super(s);
        this.textPane = textPane;
        this.list_dictionaries = list_dictionaries;
        this.getContentPane().setPreferredSize(new Dimension(W, H));
        initComponents();
        this.tree_results.setOpaque(true);   
    }
    
    public static void setConfig(int width, int height, String rootName){
         W = width;
         H = height;
         RootName = rootName;
    }
    
    private void initComponents() {
        btn_corection = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tree_results = new DynamicTree(RootName);
        select_dictionary = new JComboBox(list_dictionaries);
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btn_corection.setText("CORREGIR");

        jScrollPane2.setViewportView(textPane);
        this.getContentPane().setLayout(null);
        
        
        JPanel pComands = new JPanel();
        pComands.setPreferredSize(new Dimension(350,55));
        pComands.setLayout(null);
        
        JLabel labelSelect = new JLabel("Seleccion diccionario:");
        pComands.add(btn_corection);
        pComands.add(labelSelect);
        pComands.add(select_dictionary);
        
        Insets insets = pComands.getInsets();
        Dimension size = btn_corection.getPreferredSize();
        
        int x = insets.left;
        int y = insets.top + 25;
        btn_corection.setBounds(x, y,
                     size.width, size.height);
        x += size.width + 60;
        
        
        size = select_dictionary.getPreferredSize();
        labelSelect.setBounds(x, y-25,
                     size.width +50, size.height);
        
        size = select_dictionary.getPreferredSize();
        select_dictionary.setBounds(x, y,
                     size.width +50, size.height);
        this.add(pComands);
        this.add(jScrollPane2);
        this.add(tree_results);
        
        insets = this.getInsets();
        size = pComands.getPreferredSize();
        x =25 + insets.left;
        y = 15 + insets.top;
        pComands.setBounds(x, y,
                     size.width, size.height);
        jScrollPane2.setPreferredSize(new Dimension(350,tree_results.getPreferredSize().height));
        
        y += size.height + 15;
        size = jScrollPane2.getPreferredSize();
        jScrollPane2.setBounds(x, y,
                     size.width, size.height);
        
        //y = 5 + insets.top;
        x += size.width + 25;
        size = tree_results.getPreferredSize();
        tree_results.setBounds(x, y,
                     size.width, size.height);
        
    }

    public JComboBox getSelect_dictionary() {
        return select_dictionary;
    }
    
    public void updateTree(int[] dists, String[] words, String regex){
        String original = this.getText();
        String[] origWords = original.split(regex);
        tree_results.clear();
        for(int i=0;i<words.length;i++){
            if(dists[i] == 0){//paraula correcte
                continue;
            }
            String currentW = origWords[i];
            DefaultMutableTreeNode wordNode = tree_results.addObject(null, currentW);
            String[] options = words[i].substring(1, words[i].length()-1).split(", ");
            for (int j = 0; j < options.length;j++){
                String w = options[j].split(" ")[0];
                tree_results.addObject(wordNode,w);
            }
        }  
        tree_results.expandTreeLevel(0);      
    }

    public JButton getBtn_corection() {
        return btn_corection;
    }

    public String getText(){
        return this.textPane.getText();
    }
        
    // Variables declaration - do not modify                     
    private javax.swing.JButton btn_corection;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextPane textPane;
    private Custom.DynamicTree tree_results;
    // End of variables declaration           
}
