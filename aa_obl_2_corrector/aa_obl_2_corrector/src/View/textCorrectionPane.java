package View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;

/**
 *
 * @author Bernat Galmés Rubert
 */
public class textCorrectionPane extends JTextPane{
    private Style correctas;
    private Style erroneas;
    private DefaultStyledDocument document;

    public textCorrectionPane(Style correctas, Style erroneas, DefaultStyledDocument document) {
        super(document);
        this.correctas = correctas;
        this.erroneas = erroneas;
        this.document = document;
    }
    
    public void updateText(int[] dists, String[] words, String delimiterRegex){
        try {
            paintIncorrect(dists, delimiterRegex);
        } catch (BadLocationException ex) {
            Logger.getLogger(textCorrectionPane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void paintIncorrect(int[] dists, String delimiterRegex) throws BadLocationException{
        String text = this.getText();
        this.setText("");
        String[] origWords = text.split(delimiterRegex);//separam per espais per conservar els signes de puntuació
        int offset_text = 0;
        for(int i=0;i<dists.length;i++){
            String subText = text.substring(offset_text);
            String currentW = origWords[i];
            int i_beginWord = subText.indexOf(currentW);            
            this.document.insertString(offset_text, subText.substring(0, i_beginWord), correctas);
            offset_text += i_beginWord;
            if(dists[i] == 0){
                this.document.insertString(offset_text, currentW, correctas);

            }else{
                this.document.insertString(offset_text, currentW, erroneas);
            }
            offset_text += currentW.length();
        }   
        //perque no ens escrigui en vermell a continuació
        this.document.insertString(offset_text, " ", correctas);
    }
}
