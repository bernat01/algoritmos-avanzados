package aa_obl_2_corrector;

import Controller.Controller;
import Model.Model;
import View.View;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Efectuar una aplicación siguiendo el patrón de diseño MVC que permita la edición 
 * de un texto (símple) y que vaya marcando aquellas palabras léxicamente incorrectas. 
 * Cuando el texto se ha terminado de escribir, el programa debe de dar la posibilidad 
 * de efectuar una corrección, en donde el proceso irá proponiendo palabras y el usuario las podrá confirmar.
 * Se debe basar en la solución aportada por la Programación Dinámica para el problema de la distancia de edición.
 * El programa se debe de apoyar en sendos archivos de vocablos de dos lenguajes elegidos, y el usuario 
 * podrá elegir que idioma aplica al texto (completo) en cada caso. En esta tarea se 
 * puede encontrar un enlace al archivo con la lista de la mayoría de vocablos del idioma español. 
 * El archivo de vocablos está en formato UTF-8 sin BOM.
 * Como aportes optativos a la práctica se puede considerar una función de criterios 
 * a la hora de proponer la lista de palabras, así como probar diferentes soluciones 
 * algorítmicas de acuerdo con las diferentes funciones de distancia que se escojan 
 * (por ejemplo, nuevas operaciones, dar más valor a un cambio que a una inserción, etc). 
 * Para desarrollar distintos enfoques de la solución puede ser muy útil el DOO.
 * 
 * @author Bernat Galmés Rubert
 */
public class Aa_obl_2_corrector {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        try {
            /**
             * Configuram contingut estatic provenent del model
             */
            Controller.Config();
            View.Config();
            
            /**
             * Inici sistema
             */
            Model model = new Model();
            View view = new View(model);
            Controller controller= new Controller(model, view);
            
            view.addController(controller);
            model.addObserver(view);
            
            view.mostrar();
        } catch (Exception ex) {
            Logger.getLogger(Aa_obl_2_corrector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
