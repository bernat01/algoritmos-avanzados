package Controller;

import java.awt.image.BufferedImage;

/**
 * Classe abstracta per implementar la identificació d'una bandera a partir de les dades
 * prepocesades.
 * Implementació de la comprovació en el mètode abstracte check
 * @author Bernat Galmés Rubert
 */
public abstract class Checker {
    protected BufferedImage img;

    public Checker(BufferedImage img) {
        this.img = img;
    }
    
    public abstract float check(PreProcessing pre);
}
