package Controller;

import java.awt.image.BufferedImage;

/**
 *
 * @author bernat
 */
public class CheckerMean extends Checker{
    
    private static int Pases;
   
    public CheckerMean(BufferedImage img) {
        super(img);
    }     

    public float check(PreProcessing pre) {
        int startX = 0, startY = 0;
        int width = img.getWidth() / 3, heigth = img.getHeight() / 3;
        float dist = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int[] quadPixels = img.getRGB(startX, startY, width, heigth, null, 0, width);
                Quad q = new Quad(quadPixels, Pases);
                startX += width;
                int[] means = pre.getQuad(i, j);
                dist += (Math.abs(q.mR -  means[0])+ Math.abs(q.mG -  means[1]) + Math.abs(q.mB -  means[2]))/3;     
            }
            startY += heigth;
            startX = 0;
        }
        return dist/9;
    }
    
     public static void setConfig(int Pases) {
        CheckerMean.Pases = Pases;
    }

}
