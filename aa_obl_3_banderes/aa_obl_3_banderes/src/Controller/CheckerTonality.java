package Controller;

import java.awt.image.BufferedImage;

/**
 *
 * @author berna
 */
public class CheckerTonality extends Checker{
    private static boolean DEBUG = true;
    private static int Pases;
    
    public static void setConfig(int Pases) {
        CheckerTonality.Pases = Pases;
    }
    
    private PixelClass classifier;

    public CheckerTonality(BufferedImage img, PixelClass classifier) {
        super(img);
        this.classifier = classifier;
    }
   
    
    public float check(PreProcessing pre) {    
        int[] pixels = img.getRGB(0, 0, img.getWidth(), img.getHeight(), null, 0, img.getWidth());
        QuadTonalities q = new QuadTonalities(pixels, Pases, classifier);
        float foundPorc[] = q.getPorcTonalitats();
        float realPorcs[] = pre.getPorcTonalitats();
        float dist = 0;
        for (int i = 0; i <classifier.NumTonalities(); i++ ){
            dist += Math.abs(realPorcs[i] - foundPorc[i]);
        }
        
        return dist/classifier.NumTonalities();
    }
}
