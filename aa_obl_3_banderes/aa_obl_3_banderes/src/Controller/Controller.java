package Controller;

import Model.Message;
import Model.Model;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bernat
 */
public class Controller implements ActionListener{
    
    private static final PixelClass Classifier = new PixelClass(Model.getTonalidades());
    
    private Model model;
    private PreProcessing[] prep;
            
    public Controller(Model model) {
        this.model = model;
        this.preProccessing();
    }
    
    /**
     * Configuracio dels objectes del Controllador
     */
    public static void Config(){
        CheckerMean.setConfig(Model.getPases_mean());
        CheckerTonality.setConfig(Model.getPases_tonality());
        PreProcessing.setPases(Model.getPases_prep());
    }
    /**
     * Preprocess the images to get the mean colors in each quad
     */
    private void preProccessing(){
        BufferedImage[] images = this.model.getImages();
        this.prep = new PreProcessing[images.length];
        for (int i =0; i< images.length; i++){
            BufferedImage image = images[i];
            this.prep[i] = new PreProcessing(image, Classifier);       
        }     
    }

    /**
     * Catch the view events
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            String accio = ae.getActionCommand();
            
            switch (accio) {
                case "setMedias":
                    this.model.setCompute_tonalities(false);
                    break;
                    
                case "setTonalidades":
                    this.model.setCompute_tonalities(true);
                    break;
        
                default:
                    String[] action = accio.split("_");
                    if(action[0].equals("select")){
                        int nImg = Integer.parseInt(action[1]);
                        Checker checker;
                        if(model.getComputeTonalities()){
                            Model.debug("Computing with tonalities");
                            checker = new CheckerTonality(this.model.getImage(nImg), Classifier);
                        }else{
                            Model.debug("Computing with means");
                            checker = new CheckerMean(this.model.getImage(nImg));
                            
                        }
                        identifyFlag(checker);
                    }else{
                        throw new badSelection();
                    }
            }
            
        } catch (badSelection | InterruptedException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Identify the selected flag using the desired subclass of Checker.
     * @param checker Checker constructed with the image you are looking for
     * @throws InterruptedException 
     */
    private void identifyFlag(Checker checker) throws InterruptedException{        
        float dMin = Float.MAX_VALUE;
        int correctFlag = Integer.MAX_VALUE; 
        for (int i =0; i< this.prep.length; i++){
            this.prep[i].getThread().join();
            PreProcessing pre = this.prep[i];
            float dist = checker.check(pre);
            if(dist < dMin){
                correctFlag = i;
                dMin = dist;
            }      
        }    
        this.model.setMessage(Message.RES_FLAG + correctFlag);
    }

    private class badSelection extends Exception{}
    
}
