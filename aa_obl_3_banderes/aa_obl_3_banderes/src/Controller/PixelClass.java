package Controller;

/**
 * Agrupació de les funcions encarregades de classificar un pixel en una tonalitat
 * @author Bernat Galmés Rubert
 */
public class PixelClass {
    /**
     * Tonalitats disponibles
     */
    private Tonality Tonalidades[];
    
    /**
     * Configura la classe amb les tonalitats disponibles
     * @param TONALIDADES [[Rmax,Rmin],[Gmax,Gmin],[Bmax,Bmin]]
     * 
     */
    public PixelClass(int[][][] TONALIDADES){
        Tonalidades = new Tonality[TONALIDADES.length];
        for (int i = 0; i< TONALIDADES.length; i++){
            Tonalidades[i] = new Tonality(TONALIDADES[i]);
        }
    }
    
    /**
     * Classifica el pixel pasat per parametre amb una tonalitat
     * @param pixel
     * @return
     * @throws Exception 
     */
    public Tonality Classificar(int[] pixel) throws Exception{
        for(int i = 0; i< Tonalidades.length; i++){
            Tonality tonal = Tonalidades[i];
            if (tonal.contiene(pixel)){
                return tonal;
            }
        }
        throw new Exception("No se ha podido classificar el pixel");
    }
    
    public int NumTonalities(){
        return Tonalidades.length;
    }
}
