package Controller;

import java.awt.image.BufferedImage;

/**
 * Calcula els valors reals de les imatges utilitzats per comparar-los amb el calculat
 * @author bernat
 */
public class PreProcessing implements Runnable {
     
    private static int Pases;
  
    
    /**
     * Instance variables
     */ 
    private BufferedImage image;
    private Thread thread;
    private PixelClass classifier;
    
    private Quad[][] quads; //mitja a cada quadrant
    private float porcTonalitats[];//porcentatge de cada tonalidad guardat
     
    public PreProcessing(BufferedImage image, PixelClass classifier) {
        this.image = image;
        this.classifier = classifier;
        this.quads = new Quad[3][3];
        thread = new Thread(this);
        thread.start();
    }
    
     @Override
    public void run() {
        long time_start, time_end;
        time_start = System.currentTimeMillis();
        this.process_quads();
        process_tonalities();
        time_end = System.currentTimeMillis();
        //System.out.println("the preprocessing task has taken "+ ( time_end - time_start ) +" milliseconds");
    }
    
    
    /**
     * Carrega dins cada Quad la mitjana dels seus colors
     */
    private void process_quads() {
        int startX = 0, startY = 0;
        int width = image.getWidth() / 3, heigth = image.getHeight() / 3;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int[] quadPixels = this.image.getRGB(startX, startY, width, heigth, null, 0, width);
                this.quads[i][j] = new Quad(quadPixels, Pases);
                startX += width;
            }
            startY += heigth;
            startX = 0;
        }
    }
 
    /**
     * Carrega el porcentatge da cada tonalitat de la imatge
     */
    private void process_tonalities() {
       int[] pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
       QuadTonalities q = new QuadTonalities(pixels, classifier);
       this.porcTonalitats = q.getPorcTonalitats();        
    }


    public int[] getQuad(int fila, int col) {
        Quad q = this.quads[fila][col];
        return new int[]{q.mR, q.mG, q.mB};
    }
    
    public Thread getThread() {
        return thread;
    }

    public float[] getPorcTonalitats() {
        return porcTonalitats;
    }

    public static void setPases(int Pases) {
        PreProcessing.Pases = Pases;
    }
    
    
    
    public void debug() {
        System.out.println("Preprocess:");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Quad q = this.quads[i][j];
                q.debug();
            }
            System.out.println();
        }
    }
}
