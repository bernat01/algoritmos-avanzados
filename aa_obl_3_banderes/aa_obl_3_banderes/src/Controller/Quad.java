package Controller;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author bernat
 */
public class Quad {
    protected int[] pixels;
    
    //mean colors
    protected int mR, mG, mB;

    public Quad(int[] pixels, int pases) {
        this.pixels = pixels;
        this.compute(pases);
    }
    
    public Quad(int[] pixels) {
        this.pixels = pixels;
        this.computeAll();
    }

    public void debug(){
        System.out.print("R: "+mR+" G: "+mG+" B: "+mB+"\t");
    }
    
    //compute the porcentage of each color
    protected void compute(int pasos) {
        int sumR = 0, sumG = 0, sumB = 0;
        Color col;
        Random ran = new Random();
        for (int k = 0; k < pasos; k++) {
            int nPix = ran.nextInt(this.pixels.length);
            col = new Color(this.pixels[nPix]);
            sumR += col.getRed();
            sumG += col.getGreen();
            sumB += col.getBlue();
        }
        this.mR = sumR / pasos;
        this.mG = sumG / pasos;
        this.mB = sumB / pasos;
    }
    
    protected void computeAll() {
        int sumR = 0, sumG = 0, sumB = 0;
        Color col;
        for(int p: pixels){
            col = new Color(p);
            sumR += col.getRed();
            sumG += col.getGreen();
            sumB += col.getBlue();
        }
        this.mR = sumR / pixels.length;
        this.mG = sumG / pixels.length;
        this.mB = sumB / pixels.length;
    }

}
