package Controller;

import java.awt.Color;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Bernat Galmés Rubert
 */
public class QuadTonalities {
    
    private float porcTonalitats[];//porcentatge de cada tonalidad guardat
     
    private int[] pixels;
    private PixelClass classifier;
    
    public QuadTonalities(int[] pixels, int pases, PixelClass classifier) {
        this.pixels = pixels;
        this.classifier = classifier;
        this.compute(pases);
    }
    
    public QuadTonalities(int[] pixels, PixelClass classifier) {
        this.pixels = pixels;
        this.classifier = classifier;
        this.computeAll();
    }
    
    //compute the porcentage of each color
    protected void compute(int pasos) {
        Color col;
        Random ran = new Random();
        float sumTonalitats[] = new float[classifier.NumTonalities()];
        for (int k = 0; k < pasos; k++) {
            int nPix = ran.nextInt(pixels.length);
            col = new Color(pixels[nPix]);
            int pixel[] = {
                col.getRed(),
                col.getGreen(),
                col.getBlue()
            };
            try {
                Tonality t = classifier.Classificar(pixel);
                sumTonalitats[t.getId()] ++;
            } catch (Exception ex) {
                Logger.getLogger(CheckerTonality.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.porcTonalitats = new float[classifier.NumTonalities()];
        for (int i = 0; i <classifier.NumTonalities(); i++ ){
            this.porcTonalitats[i] = (sumTonalitats[i]/pasos);
        }
    }
    
    protected void computeAll() {
        Color col;
        float sumTonalitats[] = new float[classifier.NumTonalities()];
        for (int p : pixels){
            col = new Color(p);
            int pixel[] = {
                col.getRed(),
                col.getGreen(),
                col.getBlue()
            };
            try {
                Tonality t = classifier.Classificar(pixel);
                sumTonalitats[t.getId()] ++;
            } catch (Exception ex) {
                Logger.getLogger(CheckerTonality.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        this.porcTonalitats = new float[classifier.NumTonalities()];
        for (int i = 0; i <classifier.NumTonalities(); i++ ){
            this.porcTonalitats[i] = (sumTonalitats[i]/pixels.length);
        }
    }

    public float[] getPorcTonalitats() {
        return porcTonalitats;
    }
}
