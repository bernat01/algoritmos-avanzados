package Controller;

/**
 *
 * @author berna
 */
public class Tonality {

    private int id;
    private int[][] rangos;
    private static int Pred_id;

    public Tonality(int[][] rangos) {
        this.rangos = rangos;
        this.id = Pred_id;
        Pred_id++;
    }
    
    public boolean contiene (int[] pixel){
        
        for (int i = 0; i < 3; i++){
            if (!inRange(this.rangos[i][0], this.rangos[i][1], pixel[i])){
                return false;
            }
        }
        return true;
    }
    
    public static boolean inRange(int low, int high, int value){
        return value >= low && value <= high;
    }

    public int getId() {
        return id;
    }
}
