package Model;

import Controller.Controller;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author berna
 */
public class Model extends Observable  {
    /**
     * Contingut estatic
     */
    private static boolean DEBUG = true;
    
    //window data
    private static final String WIN_NAME = "Identificació banderes";
    private static int w = 450;
    private static int h = 450;
    
    //tamany de la botonera de banderes
    private static final int btnWidth = 75;
    private static final int btnHeigth = 50;
    
    
    // Process configuration
    private static final int pases_prep = 500; //steps to preprocess
    private static final int pases_mean = 3; // steps process with means
    private static final int pases_tonality = pases_mean*9; //steps process with tonality 
    
    //ubicació imatges
    private final static String PATH_IMAGES = "src/banderas/";
    
    /**
     * Contingut específic de la instància
     */
    private String message = "";
    
    //PROBLEM DATA 
    private BufferedImage[] images;    
    private boolean compute_tonalities = true;
   
    public Model() {
        
        String[] path_imgs = this.getImagesPaths();
        this.images = new BufferedImage[path_imgs.length];
        for (int i =0; i< path_imgs.length; i++){
            try {
                this.images[i] = ImageIO.read(new File(path_imgs[i]));
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }     
    }
   
    public static int getW() {
        return w;
    }

    public static int getH() {
        return h;
    }

    public static String getWIN_NAME() {
        return WIN_NAME;
    }

    public static int getBtnWidth() {
        return btnWidth;
    }

    public static int getBtnHeigth() {
        return btnHeigth;
    }
    
    public static int getPases_mean() {
        return pases_mean;
    }

    public static int getPases_tonality() {
        return pases_tonality;
    }

   
    public static int getPases_prep() {
        return pases_prep;
    }
    
    public boolean getComputeTonalities(){
        return this.compute_tonalities;
    }

    public void setCompute_tonalities(boolean compute_tonalities) {
        this.compute_tonalities = compute_tonalities;
    }
        
    public String[] getImagesPaths(){
        File[] files = new File(PATH_IMAGES).listFiles();
        String res[] = new String[files.length];
        for (int i=0;i<files.length;i++){
            res[i] = PATH_IMAGES + files[i].getName();
        }
        return res;
    }

    public BufferedImage[] getImages() {
        return images;
    }   

    public BufferedImage getImage(int i){
        return this.images[i];
    }

     public void setMessage(String message) {
        this.message = message;
        
        this.setChanged();
        this.notifyObservers();
    }

    public String getMessage() {
        return message;
    }
    
   
    public static int[][][] getTonalidades(){
        return Tonalities.getTONALIDADES();
    }
    
    public static void debug(String str){
        if(DEBUG){
            System.out.println(str);
        }
    }
    
}
