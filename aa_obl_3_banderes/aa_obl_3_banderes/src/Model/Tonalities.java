package Model;

/**
 * Classe que conté els rangs dels components RGB de les diferents tonalitats
 *
 * @author Bernat Galmés Rubert
 */
public class Tonalities {

    private static final int[][][] TONALIDADES
            = {
                {//BLACK
                    {0, 127},// R range
                    {0, 127},// G range
                    {0, 127} // B range
                },//WHITE
                {
                    {128, 255},
                    {128, 255},
                    {128, 255}
                },//GREEN
                {
                    {0, 127},
                    {128, 255},
                    {0, 127}
                },//RED
                {
                    {128, 255},
                    {0, 127},
                    {0, 127}
                },//BLUE
                {
                    {0, 127},
                    {0, 127},
                    {128, 255}
                },//TURQUESA
                {
                    {0, 127},
                    {128, 255},
                    {128, 255}
                },//VIOLETA
                {
                    {128, 255},
                    {0, 127},
                    {128, 255}
                },//YELLOW
                {
                    {128, 255},
                    {128, 255},
                    {0, 127}
                }
            };

    public static int[][][] getTONALIDADES() {
        return TONALIDADES;
    }

}
