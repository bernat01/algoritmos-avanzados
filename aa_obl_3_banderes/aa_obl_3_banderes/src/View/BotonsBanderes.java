package View;

import java.awt.Image;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 *
 * @author bernat
 */
public class BotonsBanderes extends JPanel{
    private static int btnWidth;
    private static int btnHeigth;
    
    private String[] images;
    private JButton[] botones;

    
    
    public BotonsBanderes(String[] images) {
        this.images = images;
        this.botones = new JButton[images.length];        
        this.setLayout(null);
        this.placeButtons();        
    }
    
    public static void Config(int btnW, int btnH){
        btnWidth = btnW;
        btnHeigth = btnH;
    }
    
    
    private void placeButtons(){
        for(int i = 0;i< images.length;i++){
            String path_image = this.images[i];
            ImageIcon btnIcon = new ImageIcon(path_image);
            Image img = btnIcon.getImage();
            img = img.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
            btnIcon = new ImageIcon(img);
            
            botones[i] = new JButton();
            botones[i].setBounds(0, i*btnHeigth, btnWidth, btnHeigth);
            
            initButton(botones[i], btnIcon);
            this.add(this.botones[i]);
        }
    }
    
    private void initButton(JButton but, ImageIcon icon){
        but.setIcon(icon);
        but.setContentAreaFilled(false);
        but.setRolloverEnabled(true);
   }
    
    public void addController(ActionListener controller) {
         for(int i = 0;i< botones.length;i++){
            JButton btn = botones[i];
            btn.setActionCommand("select_"+i);
            btn.addActionListener(controller);
        }
     }
    
}
