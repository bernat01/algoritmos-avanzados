package View;

import Model.Model;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;

/**
 *
 * @author berna
 */
public class View implements Observer{
    
    private Model model;
    private Window gui;
    
    public View(Model model) {
        this.model = model;      
        gui = new Window(this.model.getWIN_NAME(), this.model.getImagesPaths());
    }
    
    public static void Config(){
        Window.setDimensions(Model.getW(), Model.getH());
        BotonsBanderes.Config(Model.getBtnWidth(), Model.getBtnHeigth());
    }

    public void addController(ActionListener controller) {
       this.gui.getPanelButtons().addController(controller);
       this.gui.getRadio_medias().setActionCommand("setMedias");
       this.gui.getRadio_medias().addActionListener(controller);
       this.gui.getRadio_tonalities().setActionCommand("setTonalidades");
       this.gui.getRadio_tonalities().addActionListener(controller);
     }
     
    public void mostrar() {
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.pack();
        gui.setVisible(true);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        this.gui.getLabel_results().setText(model.getMessage());
    }
}
