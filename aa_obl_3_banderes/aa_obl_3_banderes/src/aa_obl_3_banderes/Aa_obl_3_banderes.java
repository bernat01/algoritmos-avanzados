package aa_obl_3_banderes;

import Controller.Controller;
import Model.Model;
import View.View;

/**
 * @author bernat
 */
public class Aa_obl_3_banderes {

    /**
     * Descripción:
     * Se trata de generar un programa que usando técnicas probabilísticas sea capaz de distinguir por colorimetría 
     * una bandera de un conjunto maestro de banderas. 
     * Es decir, dada una imagen de una bandera (sintética), sea capaz de decirnos de que bandera se trata. 
     * El programa debe de seguir como siempre el patrón MVC. 
     * Y debe de clasificar cada pixel al color más cercano a un rango de colores que representa el ojo humano. 
     * Por ejemplo, se deben de tener varios tonos de rojo, 
     * entonces si el pixel tratado se parece más que a otros colores a algún tono de rojo se le asignará el color rojo. 
     * Si esta técnica se lleva a cabo con el cubo de color que corresponde al arcoiris más el blanco y el negro, 
     * la dispersión colorimétrica de las diferentes imágenes es muy efectiva.
     * Como característica adicional que refuerza la efectividad del algoritmo se puede contrastar 
     * también el area central de las imágenes, a fin de mejorar la precisión de detección.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /**
         * Configuram contingut estatic provenent del model
         */
        Controller.Config();
        View.Config();
        
        /**
         * Inici sistema
         */
        Model model = new Model();
        View view = new View(model);
        Controller controller= new Controller(model);
        
        view.addController(controller);
        model.addObserver(view);
        
        view.mostrar();
    }
    
}
