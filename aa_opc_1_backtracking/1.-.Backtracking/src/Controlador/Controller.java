package Controlador;

import Modelo.Modelo;
import Modelo.Piezas.Pesa;
import Modelo.Piezas.Pesa.badSelection;
import Vista.Finestra;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author bernat
 */
public class Controller implements ChangeListener, ActionListener {

    private Finestra view;
    private final Modelo modelo;
    
    public Controller(Modelo modelo, Finestra win) {
        this.modelo = modelo;
        this.view = win;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            String accio = ae.getActionCommand();
            switch (accio) {
                case "addPiece":      
                    Pesa p_sel = Pesa.getPesa(view.getCurrentPiezeSelected());
                    this.modelo.addPiece(p_sel);
                    break;
                    
                case "compute":
                    Process sol = new Process(modelo);
                    view.reset();
                    sol.start();                    
                    break;
                    
                case "resetPieces":
                    this.modelo.removePieces();
                    break;
                    
                default:
                    throw new badSelection();
            }
            
        } catch (badSelection ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //Es crida quan canviam el valor de l'spinner amb el tamany del tauler
    @Override
    public void stateChanged(ChangeEvent ce) { // TODO add your handling code here:
        javax.swing.JSpinner sp_boardSize = (javax.swing.JSpinner) ce.getSource();
        int newSize = (int) sp_boardSize.getValue();
        modelo.setBoardSize(newSize);
    }
}
