package Controlador;

import Modelo.Modelo;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author bernat
 */
public class Process extends Thread {

    private final Modelo modelo;
    
    public Process(Modelo modelo) {
        this.modelo = modelo;
    }
    
    @Override
    public void run() {
        System.out.println("Empezamos el calculo, piezas: " + modelo.numPieces());
        try {
            Solution sol = new Solution(modelo.getPieces(), modelo.boardSize());
            Timer timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                @Override
                public void run() {
                    modelo.setProgress(sol.porcComputed());
                }
            }, 500, 500);
            int[][] t_sol = sol.findSolution();                   
            timer.cancel();
            
            if (t_sol != null){
                for (int i = 0; i < t_sol.length; i++) {
                    for (int j = 0; j < t_sol.length; j++) {
                        int c_cas = t_sol[i][j];
                        if (c_cas != 0 && c_cas != 1 && c_cas != -1) {
                            this.modelo.setPesa(t_sol[i][j], j, i);
                        }
                    }
                }
            }
            modelo.setLastExecutionOK(t_sol != null);
        } catch (StackOverflowError e) {
            System.err.print("\nHemos superado el límite de la pila");
        }
    }

    
}
