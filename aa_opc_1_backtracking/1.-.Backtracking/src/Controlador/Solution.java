/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Piezas.Pesa;
import java.math.BigInteger;
import pkg1.backtracking.Utils;

/**
 *
 * @author bernat
 */
public class Solution {
    
    private Pesa[] peses;
    private int boardSize;

    
    /**
     * Estimacio del nombre de estats màxim
     */
    private BigInteger numColocacions;
    
    /**
     * Nombre estats provats
     */
    private long provadas;
    
    /**
     * Indica que s'ha trobat una solució bona
     */
    boolean completa = false;
    
    public Solution(Pesa[] peses, int boardSize) {
        this.peses = peses;
        this.boardSize = boardSize;
        //this.numColocacions = BigInteger.valueOf(modelo.numPieces()).multiply(Utils.factorial(BigInteger.valueOf(modelo.boardSize()*modelo.boardSize())));
        this.numColocacions = BigInteger.valueOf(peses.length/4).multiply(Utils.factorial(BigInteger.valueOf(boardSize)));
        this.provadas = 1;
    }
    
    public int[][] findSolution(){
        int piezaactual = peses.length - 1;
        Status e = backtracking(piezaactual, new Status(boardSize));
        if (e == null){
            return null;
        }
        return e.getTablero();
    }
     
    /**
     * @return Porcentatge dels calculs realitzats
     */
    public double porcComputed(){
        return ((double)this.provadas/this.numColocacions.doubleValue())*100;
    }
    
    
    /**
     * Calcula una colocació de les peces que tenim al model 
     * de manera que no es matin
     * @param piezaactual peça que colocam (cridar per la darrera peça de la llista del model)
     * @param estadoactual estat del tauler en el moment de la crida
     * @return L'estat resultant després de colocar 'piezaactual
     */
    private Status backtracking(int piezaactual, Status estadoactual) {
        if(ultimaPieza(piezaactual)){
            completa = true;
            return estadoactual;
        }
        
        Pesa pactual = peses[piezaactual];
        Status solucion = null;
        while (estadoactual.isOK() && !completa) {
            int[][] nouTauler = estadoactual.colocarPesa(pactual);
            Status nextestado = new Status(nouTauler);
            //System.out.println("pieza: "+piezaactual);
            if (estadoactual.isOK()) {// Pieza colocada
                //colocamos siguiente pieza
                solucion = backtracking(piezaactual-1, nextestado);           
            }
        }
        this.provadas++;
        
        return solucion;
    }
    
    private static boolean ultimaPieza(int piezaactual) {
        return piezaactual == -1;
    }
    
   
    
}
