/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Piezas.Pesa;
import Modelo.Piezas.Pesa.T_mov;

/**
 *
 * @author alvaro
 */
public class Status{
    //POSIBLES VALORS DE LES CASELLES DEL TAULER
    private static final int POS_PROBADA = -1;
    private static final int POS_LLIURE = 0;
    private static final int POS_AMENASA = 1;
    
    //ATRIBUTOS
    private int[][] tablero;
    private boolean hihaSolucio;
    
    //MÉTODOS CONSTRUCTORES
    public Status(int n) {
        this.tablero = new int[n][n];
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero.length; j++) {
                tablero[i][j] = POS_LLIURE;
            }
        }
        this.hihaSolucio=true;
    }

    public Status(int[][] tablero) {
        this.tablero = tablero;
        this.hihaSolucio=true;
    }

    public boolean isOK(){
        return this.hihaSolucio;
    }
   
    /**
     * Coloca la pieza p en una posición no amenazada del tablero
     * @param p Pieza a colocar
     * @return Status del tablero despues de colocar la pieza
     */
    public int[][] colocarPesa(Pesa p) {
        Posicio pos;
        int[][] nuevoTablero = null;

        /* buscamos una posición para la ficha y, por cada posición comprobamos
         que no amenaza ni es amenazada */
        this.hihaSolucio = true;
        do {
            //buscamos una posicion disponible
            pos = nextPosibleSolucion();
            if (pos == null) {
                this.hihaSolucio = false;
                return null;
            }
            
            //comprovamos si es buena
            nuevoTablero = actualizarAmenazas(pos, p);
        } while ((nuevoTablero == null));//mientras posicion amenazada
        
        return nuevoTablero;
    }
    
    /**
     * Busca una posición del tablero no comprobada
     * La marca como comprobada
     * @return Posicion disponible, null si no quedan posiciones sin comprovar
     */
    private Posicio nextPosibleSolucion() {
        for (int i = 0; i < this.tablero.length; i++) {
            for (int j = 0; j < this.tablero.length; j++) {
                if (this.tablero[i][j] == POS_LLIURE) {
                    /* marcamos esta opción como usada */
                    this.tablero[i][j] = POS_PROBADA;

                    /* devolvemos la posición */
                    return new Posicio(i,j);
                }
            }
        }
        return null;
    }
    
    /**
     * 
     * @param pos
     * @param p
     * @return 
     */
    private int[][] actualizarAmenazas(Posicio pos, Pesa p) {
        int[] movx = p.getMovX();
        int[] movy = p.getMovY();
        boolean amenaza = false;
        int eje_x = 0;
        int eje_y = 0;
        int[][] nuevotablero = new int[this.tablero.length][];
        
        //clonamos el tablero
        for (int i = 0; i < tablero.length; i++) {
            nuevotablero[i] = tablero[i].clone();
        }
        if (p.getT_mov() == T_mov.infinit) {
            //aplicamos los movimientos de la pieza
            outerloop:
            for (int i = 0; (i < movx.length); i++) {
                //movimiento
                eje_x = pos.x + movx[i];
                eje_y = pos.y + movy[i];
                //para estas fichas miramos toda la longitud del tablero
                while ((eje_x >= 0) && (eje_y >= 0) && (eje_x < nuevotablero.length) && (eje_y < nuevotablero.length)) {
                    if(!checkPos(eje_x, eje_y, nuevotablero)){
                        amenaza = true;
                        break outerloop;
                    }
                    
                    //siguiente casilla amenazada
                    eje_x = eje_x + movx[i];
                    eje_y = eje_y + movy[i];
                }
            }
        } else {
            //int i recorre el vector de movimientos, eje_x y eje_y son las coordenadas de las cassillas resultantes del movimiento
            for (int i = 0; (i < movx.length); i++) {
                //movimiento
                eje_x = pos.x + movx[i];
                eje_y = pos.y + movy[i];
                //si aplicando el movimiento seguimos dentro del tablero...
                if ((eje_x >= 0) && (eje_y >= 0) && (eje_x < nuevotablero.length) && (eje_y < nuevotablero.length)) {
                    if(!checkPos(eje_x, eje_y, nuevotablero)){
                        amenaza = true;
                        break;
                    }
                }
            }
        }

        if (!amenaza) {
            //escribimos la ficha en el tablero
            nuevotablero[pos.x][pos.y] = p.getCodi();
            return nuevotablero;
        }
        return null;
    }
    
    private boolean checkPos(int x, int y, int[][] tauler){
        if (tauler[x][y] > 1) {//en la casilla hay una pieza
            return false;
        } 
        //marcamos la casilla como amenazada.
        tauler[x][y] = POS_AMENASA;
        return true;
    }

    //
    //MÉTODOS DE APOYO
    //
    public int[][] getTablero() {
        return tablero;
    }

    /**
     * Mètode que imprimeix el tauler per pantalla
     */
    public void debug(){
        System.out.println("DEBUG STATUS");
        for (int i = 0; i < this.tablero.length; i++) {
            System.out.print("["+i+"]");
            for (int j = 0; j < this.tablero.length; j++) {
                System.out.print(this.tablero[i][j]);
            }
            System.out.println();
        }
    }
    
    
    private class Posicio {

        public int x;
        public int y;

        public Posicio() {
        }

        public Posicio(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }
}
