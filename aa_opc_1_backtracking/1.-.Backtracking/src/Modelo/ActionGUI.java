package Modelo;

/**
 *
 * @author bernat
 */
public class ActionGUI {
    public String name;
    public Object data;

    public ActionGUI(String name, Object data) {
        this.name = name;
        this.data = data;
    }
    
}
