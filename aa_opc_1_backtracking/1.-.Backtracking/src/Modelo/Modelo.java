package Modelo;

import Modelo.Piezas.Pesa;
import java.util.ArrayList;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author berna
 */
public class Modelo extends Observable  {
    private static final int INI_BOARD_SIZE = 8;
    
    private static final String[] T_PECES = {"Alfil", "Cagat", "Cavall", "Gat", "Rey", "Reina", "Torre"};
    
    private int boardSize = INI_BOARD_SIZE;
    private double progress;
    private ArrayList<Pesa> pieces;
    private boolean lastExecutionOK;

    public Modelo() {
        this.pieces = new ArrayList<>();//aquí se guardan las fichas a colocar
    }
       
    public void setBoardSize(int n){
        //this.t.setSize(n);
        this.boardSize = n;
        //notificamos a la vista que ha cambiado el tamaño del tablero
        this.setChanged();
        this.notifyObservers(new ActionGUI("size", this.boardSize));
    }
    
    
     public void addPiece(Pesa p) {
        this.pieces.add(p);

        //notificamos a la vista que se ha añadido una pieza
        this.setChanged();
        this.notifyObservers(new ActionGUI("newPiece", p));
    }
     
    public void removePieces(){
        this.pieces.clear();
        
        this.setChanged();
        this.notifyObservers(new ActionGUI("removePieces", null));
    }
     
    public int boardSize(){
        return this.boardSize;
    }

    public double getProgress() {
        return progress;
    }

    public void setProgress(double progress) {
        this.progress = progress;
        
        //notificamos a la vista que se ha añadido una pieza
        this.setChanged();
        this.notifyObservers(new ActionGUI("updateProgress", this.progress));
        
    }

    public Pesa[] getPieces() {
        Pesa[] pes = new Pesa[pieces.size()];
        pieces.toArray(pes);
        return pes;
    }
   
    
    public int numPieces() {
        return this.pieces.size();
    }

    public Pesa getPiece(int i) {
        return this.pieces.get(i);
    }
    
    public void setPesa(int codiPesa, int col, int fila){
        try {
            Pesa p = Pesa.getPesa(codiPesa);
            p.setPosition(fila, col);
            
            //notificamos a la vista que se ha colocado una pieza
            this.setChanged();
            Object[] data = new Object[]{col, fila, p};
            this.notifyObservers(new ActionGUI("casella", data));
        } catch (Exception ex) {
            Logger.getLogger(Modelo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    public static int getINI_BOARD_SIZE() {
        return INI_BOARD_SIZE;
    }

    public void setLastExecutionOK(boolean lastExecutionOK) {
        this.lastExecutionOK = lastExecutionOK;
        
        
        String msg = (lastExecutionOK) ? Missatges.OK: Missatges.BAD;

        this.setChanged();
        this.notifyObservers(new ActionGUI("message", msg));
    }
    
    private static class Missatges{
        final static String OK = "Resultat trobat";
        final static String BAD = "No s'han trobat resultats";
    }

    public static String[] getT_PECES() {
        return T_PECES;
    }
    
    
}
