package Modelo.Piezas;

import java.awt.Image;
import java.awt.Toolkit;


/**
 *
 * @author bernat
 */
public class Alfil extends Pesa {

    public Alfil() {
        this.t_mov = T_mov.infinit;
        this.MovX= new int[]{+1, -1, -1, +1};
        this.MovY = new int[]{+1, +1, -1, -1};
        this.codi = 4;
    }
        
    @Override
    public Image FiguraFBlanc() {
         Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Blanc/Alfil.gif");
    }

    @Override
    public Image FiguraFNegre() {
         Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Negre/Alfil.gif");
    }
    
    @Override
    public String toString() {
        return "Alfil";
    }
}
