/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo.Piezas;

import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author Bernardo
 */
public class Gat extends Pesa {

    public Gat() {
        this.t_mov = T_mov.infinit;
        MovX = new int[]{0, 0};
        MovY = new int[]{-1, +1};
        this.codi = 7;
    }

    @Override
    public Image FiguraFBlanc() {
        Toolkit t = Toolkit.getDefaultToolkit();
        return t.getImage("peces/Fons_Blanc/Gat.gif");
    }

    @Override
    public Image FiguraFNegre() {
        Toolkit t = Toolkit.getDefaultToolkit();
        return t.getImage("peces/Fons_Negre/Gat.gif");
    }

    @Override
    public String toString() {
        return "Gat";
    }

}
