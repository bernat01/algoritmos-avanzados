package Modelo.Piezas;

import java.awt.Image;

public abstract class Pesa {

    protected int MovX[];
    protected int MovY[];
    protected int codi;
    protected T_mov t_mov;
    
    protected int[] position = new int[]{-1, -1};
    
    
    public abstract Image FiguraFBlanc();
    public abstract Image FiguraFNegre();
    
    
    public void setPosition(int fila, int columna){
        position = new int[]{fila, columna};
    }
    
    public boolean estaColocada(){
        return position[0] != -1;
    }

    public int[] getPosition() {
        return position;
    }
    
    public int[] getMovX() {
        return MovX;
    }

    public int[] getMovY() {
        return MovY;
    }

    public int getCodi() {
        return codi;
    }
    
    public T_mov getT_mov(){
        return this.t_mov;
    }

    @Override
    public String toString() {
        return "Pesa{" + "MovX=" + MovX + ", MovY=" + MovY + '}';
    }
    
    ///
    ///Funcions estàtiques de Pesa
    ///
    
    
    public static enum T_mov{
        unari, infinit
    }
    
    public static Pesa getPesa(int codi) throws Exception {
        switch (codi) {
            case 2:
                return new Cavall();
            case 3:
                return new Reina();
            case 4:
                return new Alfil();
            case 5:
                return new Torre();
            case 6:
                return new Rei();
            case 7:
                return new Gat();
            case 8:
                return new Cagat();
            default:
                throw new badSelection();
        }
    }
    
     public static Pesa getPesa(String t_pesa) throws badSelection {
        switch (t_pesa) {
            case "Alfil":
                return new Alfil();
                
            case "Cagat":
                return new Cagat();
                
            case "Cavall":
                return new Cavall();
                
            case "Gat":
                return new Gat();
                
            case "Rey":
                return new Rei();
                
            case "Reina":
                return new Reina();
                
            case "Torre":
                return new Torre();
                
            default:
                throw new badSelection();
        }  
    }
       public static class badSelection extends Exception{
    
    }
}
