package Modelo.Piezas;

import java.awt.Image;
import java.awt.Toolkit;


/**
 *
 * @author bernat
 */
public class Rei extends Pesa {
    
    public Rei() {
        this.t_mov = T_mov.unari;
        MovX = new int[]{+1, -1, -1, +1,  0,  0, +1, -1};
        MovY = new int[]{+1, -1, +1, -1, +1, -1,  0,  0};
        this.codi = 6;
    }

   /* public static boolean MaxMoviments(int nMov){
        return nMov== NumMoviments;
    }*/
    
    
  @Override
    public Image FiguraFBlanc() {
         Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Blanc/Rei.gif");
    }

    @Override
    public Image FiguraFNegre() {
         Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Negre/Rei.gif");
    }

    @Override
    public String toString() {
        return "Rei";
    }
    
}
