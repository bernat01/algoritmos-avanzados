package Modelo.Piezas;

import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author bernat
 */
public class Reina extends Pesa{
    
    public Reina() {
        this.t_mov = T_mov.infinit;
        MovX = new int[]{+1, -1, -1, +1,  0,  0, +1, -1};
        MovY = new int[]{+1, -1, +1, -1, +1, -1,  0,  0};
        this.codi = 3;
    }
    
   @Override
    public Image FiguraFBlanc() {
        Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Blanc/Reina.gif");
    }

    @Override
    public Image FiguraFNegre() {
         Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Negre/Reina.gif");
    }

    @Override
    public String toString() {
        return "Reina";
    }
}
