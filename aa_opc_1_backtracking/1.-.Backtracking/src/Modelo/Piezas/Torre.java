package Modelo.Piezas;

import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author bernat
 */
public class Torre extends Pesa{

    public Torre() {
        this.t_mov = T_mov.infinit;
        MovX = new int[]{+1, -1,  0,  0};
        MovY = new int[]{ 0,  0, -1, +1};
        this.codi = 5;
    }
    
    @Override
    public Image FiguraFBlanc() {
         Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Blanc/Torre.gif");
    }

    @Override
    public Image FiguraFNegre() {
        Toolkit t = Toolkit.getDefaultToolkit ();
        return t.getImage("peces/Fons_Negre/Torre.gif");
    }
    @Override
    public String toString() {
        return "Torre";
    }
}
