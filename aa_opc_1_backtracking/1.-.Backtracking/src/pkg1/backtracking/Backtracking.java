package pkg1.backtracking;

import Controlador.Controller;
import Modelo.Modelo;
import Vista.Finestra;


/**
* Backtracking.
* Ejercicio: Posicionamiento de las n reinas en un tablero de nxn.
* Se trata de realizar un programa que teniendo definidas 5 piezas 
* (o más; reina, torre, alfil y dos piezas inventadas por el programador),
* sea capaz de colocar en el tablero de nxn las n piezas iguales, sin que se maten. 
* Si no es posible, el programa debe de informar de ello
* El programa elaborado debe de presentar una estructura de diseño MVC. 
* Debe además tener diseñadas las piezas en una jerarquía objeto
*   (Pieza extiende --> {caballo, torre, alfil, …})
*   El backtracking maneja pieza
*   De esta forma es sencillo añadir piezas nuevas
* 
* El programa debe presentar una IGU gráfica y estar desarrollado con el uso de las 
* clases de Observer y Observable de Java.
* En caso de usar otro lenguaje objeto usar clases equivalentes o construirse un interface similar al uso.
* NO vale el esquema básico visto al inicio de curso. 
*/

/**
 *
 * @author bernat
 */
public class Backtracking {


    public Backtracking() {
        Modelo modelo = new Modelo();
        Finestra win = new Finestra(Modelo.getINI_BOARD_SIZE(), Modelo.getT_PECES()); //vista-> observer
        
        //Aqui se Agregan los observadores
        modelo.addObserver(win);
        
        Controller events = new Controller(modelo, win);
        win.addController(events);
        //tauler
        win.setVisible(true);
    }

    public static void main(String[] args) {
        Backtracking m = new Backtracking();
    }

}
