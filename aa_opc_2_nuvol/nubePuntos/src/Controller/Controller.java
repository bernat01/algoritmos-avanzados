/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Model;
import View.View;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bernat
 */
public class Controller implements ActionListener{
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }
    

    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            String endMessage = "El calculo con ";
            //afegim pesa
            String accio = ae.getActionCommand();
            Proces sol;
            switch (accio) {
                case "compute_n2":
                    model.init_n2();
                    endMessage += "n2";
                    break;
                    
                case "compute_nlogn":
                    model.init_nlogn();
                    endMessage += "nlogn";
                    break;
                default:
                    throw new badSelection();
            }
            endMessage += " ha terminado con exito";
            
            CallBack cb = new CallBack(endMessage);
            sol = new Proces(accio, model, cb);         
            Thread t = new Thread(sol);
            t.start();
            
        } catch (badSelection ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public class CallBack{
        private String message;

        public CallBack(String message) {
            this.message = message;
        }
        
        public void callback(){
            view.showAlert(this.message);
        }
    }
    private class badSelection extends Exception{}
    
}
