package Controller;

import Controller.Controller.CallBack;
import Model.Model;

public class Proces implements Runnable {    
    private static final boolean DEBUG=false;
    
    private String t_calculs;
    private Model model;
    private CallBack cb;
    
    
    public Proces(String t_calculs, Model model, CallBack cb) {
        this.t_calculs = t_calculs;
        this.model = model;
        this.cb = cb;
    }
     
    @Override
    public void run() {
        
        int nPoints = model.getN_INIT();
        for (int i = 0; i < model.getN_ITER(); i++){
            int[][] cloud = model.getCloud(nPoints);
            if(DEBUG){
               double r1 = compute_n2(cloud);
               double r2 = compute_nlogn(cloud, 0, cloud.length-1);
               System.out.println("nPoints: "+nPoints+"n2: "+r1+" nlogn: "+r2);
               if(r1!= r2){
                   System.err.println("error");
               }
               nPoints += model.getINCREMENT();
               continue;
            }
            if(t_calculs == "compute_n2"){
                long startTime = System.nanoTime();
                compute_n2(cloud);
                long endTime = System.nanoTime();
                long elapsed = (endTime-startTime);
                model.addTime_n2(elapsed);
                
                
            }else if(t_calculs == "compute_nlogn"){
                long startTime = System.nanoTime();
                double r2 = compute_nlogn(cloud, 0, cloud.length-1);
                long endTime = System.nanoTime();
                long elapsed = (endTime-startTime);
                model.addTime_nlogn(elapsed);
                
            }
            nPoints += model.getINCREMENT();
            //TODO: ACTUALITZAR PROGRES
        }
        cb.callback();
    }
    
    private double compute_n2(int[][] points){
        double dMin = Double.MAX_VALUE;
        //int[] p1min, p2min;
        for (int i=0; i< points.length; i++){
            int[] pi = points[i]; 
            for (int j=0;j< points.length; j++){
                if (i == j) break;
                int[] pj = points[j];
                double dist = dist(pi, pj);
                if (dist < dMin){
                    dMin = dist;
                   // p1min = pi;
                   // p2min = pj;
                }                
            }
        }
        return dMin;
    }
    
    private double dist(int[] p1, int[] p2){
        double a, b;
        a = Math.pow(p1[0] - p2[0], 2);
        b = Math.pow(p1[1] - p2[1], 2);
        return Math.sqrt(a+b);
    }
    
    private double compute_nlogn(int[][] points, int ini, int fi){
        if (fi == ini){
            return Double.MAX_VALUE;
            
        }else if(fi == ini +1){
            return dist(points[ini], points[fi]);
        }
                
        double sol1 = compute_nlogn(points, ini, (fi+ini)/2);
        double sol2 = compute_nlogn(points, ((fi+ini)/2), fi);
        
        //fusionam els resultats
        double dMin = (sol1 < sol2) ?  sol1 : sol2;     
        return dMin;
        
    }
    
}