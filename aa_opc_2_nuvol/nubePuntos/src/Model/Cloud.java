/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Random;

/**
 *
 * @author bernat
 */
public class Cloud {
    final int RAND_SEED = 100;
    private int[][] points;
    private int nPoints;

    public Cloud(int nPoints) {
        this.nPoints = nPoints;
        this.points = new int[this.nPoints][2];
        this.randomPoints();
    }
    
    @SuppressWarnings("empty-statement")
    private void randomPoints(){
        Random numGen = new Random(RAND_SEED);
        //Random numGen = new Random(nPoints);
        int prevX = 0;
        int prevY = 0;
        for (int i =0; i<this.nPoints; i++){
            int x = numGen.nextInt(100)+prevX;
            int y = numGen.nextInt(100)+prevY;
            this.points[i] = new int[]{x, y};
            
            prevX = x +1;
            prevY= y+1;
        }
    }

    public int[][] getPoints() {
        return points;
    }
    
}
