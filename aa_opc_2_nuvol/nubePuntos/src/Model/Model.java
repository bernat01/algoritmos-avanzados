/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Observer;

/**
 *
 * @author berna
 */
public class Model {
    //configuración de las nubes
    private final int N_ITER = 75;
    private final int N_INIT = 100;
    private final int INCREMENT = 200;
        
    //resultados de los calculos
    private Times times_n2;
    private Times times_nlogn;

    public Model() {        
        this.times_n2 = new Times();
        this.times_nlogn = new Times();
    }

    public int getINCREMENT() {
        return INCREMENT;
    }

    public int getN_INIT() {
        return N_INIT;
    }

    public int getN_ITER() {
        return N_ITER;
    }  
    
    public int[][] getCloud(int nPoints){
        Cloud c = new Cloud(nPoints);
        return c.getPoints();
    }
    
    public void initObservers(Observer panel){
        times_n2.addObserver(panel);
        times_nlogn.addObserver(panel);
    }
    
    public void init_n2(){
        times_n2.reset();
    }
    
    public void init_nlogn(){
        times_nlogn.reset();
    }
    
    public void addTime_n2(long time){
        this.times_n2.addTime(time);
    }
    
    public void addTime_nlogn(long time){
        this.times_nlogn.addTime(time);
    }

    public ArrayList<Long> getTimes_n2() {
        return times_n2.getTimes();
    }

    public ArrayList<Long> getTimes_nlogn() {
        return times_nlogn.getTimes();
    }
    
}