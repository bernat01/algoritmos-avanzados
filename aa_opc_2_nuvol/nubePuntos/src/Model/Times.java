/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author bernat
 */
public class Times  extends Observable {
    private ArrayList<Long> times;
    
    public Times(){
        times = new ArrayList<Long>();
    }
    
    public void addTime(long time){
        times.add(time);
        this.setChanged();
        this.notifyObservers(time);
    }

    public ArrayList<Long> getTimes() {
        return times;
    }
    
    public void reset(){
        times.clear();
    }
        
}
