package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;

/**
 *
 * @author bernat
 */
public class Panel_results extends JPanel implements Observer {

    private static final int MAXX = 600, MAXY = 800;
    //private PointPanel lastPoint;
    private View view;
    
    public Panel_results(View view) {
        this.view = view;
        this.setPreferredSize(new Dimension(MAXX, MAXY));
        setBackground(Color.WHITE); 
    }
            
    /**
     * Mètode que pinta el tauler del joc
     *
     * @param g Objecte per a pintar els components del tauler
     */
    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        super.paintComponent(g);
        paintTimes(g2d, this.view.getTimes_n2());
        paintTimes(g2d, this.view.getTimes_nlogn());
        
    }
    
    private void paintTimes(Graphics2D g2d, ArrayList<Long> times){
        if (times.isEmpty()){
            return;
        }
        int x_inc = MAXX / times.size();
        PointPanel prevP = new PointPanel(0,0);
        for(int i = 0; i< times.size(); i++){
            double time = times.get(i)/100000;//TODO: a vegades aixeca un null pointer exception
            PointPanel p = new PointPanel(prevP.x+x_inc, (int)time/4);
            g2d.fillOval(p.x, p.y, 5, 5);
            g2d.drawLine(prevP.x, prevP.y, p.x, p.y);
            prevP = p;
            System.out.println("tiempo: "+time);
        } 
    }
    @Override
    public void update(Observable o, Object o1) {
        repaint();
    }
    
    private class PointPanel{
        public int x, y;

        public PointPanel(int x, int y) {
            this.x = x;
            this.y = y;
        }
        
    }
}
