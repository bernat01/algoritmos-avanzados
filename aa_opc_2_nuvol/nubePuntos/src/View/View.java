/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.Controller;
import Model.Model;
import java.util.ArrayList;
import java.util.Observer;
import javax.swing.JOptionPane;

/**
 *
 * @author berna
 */
public class View {
    private Window_main win;
    private Panel_results panel_res;
    private Model model;
            
    public View(Model model) {
        this.model = model;
        this.win = new Window_main(this);
        this.panel_res = (Panel_results) win.getPanel_results();
    }
    
    public void init(Controller events_controller){
        win.addController(events_controller);

        win.setVisible(true);
    }
 
    public Observer getObserver(){
        return this.panel_res;
    }
    
    public void showAlert(String text){
        JOptionPane.showMessageDialog(null, text);
    }

    public ArrayList<Long> getTimes_n2() {
        return model.getTimes_n2();
    }

    public ArrayList<Long> getTimes_nlogn() {
        return this.model.getTimes_nlogn();
    }
}
