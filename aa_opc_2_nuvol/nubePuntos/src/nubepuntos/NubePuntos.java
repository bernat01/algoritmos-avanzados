package nubepuntos;

import Controller.Controller;
import Model.Model;
import View.View;

/**
 * D&C. Mínima distancia entre dos puntos cualquiera de una nube de puntos
 * 
 * Se trata de realizar un programa que dada una nube de puntos repartidos en 
 * una superficie por una distribución equiprobable continua, 
 * sea capaz de averiguar la mínima distancia existente (entre dos puntos) en esa nube.
 * 
 * El programa elaborado debe de presentar una estructura de diseño MVC. 
 * Debe además poder solucionar el problema con un coste computacional asintótico de O(n2) y también con un coste de O(n·log n)
 * Debe poder pintar una gráfica que muestre las tendencias computacionales de los dos algoritmos mediante una simulación.
 * La implementación del MVC debe de hacer uso de las clases Observer y Observable )o similares en ptro lenguaje).
 * Se debe de implementar una GUI.
 * 
 * Fecha tope 26 de mayo.
 * @author Bernat Galmés Rubert
 */
public class NubePuntos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Model model = new Model();
        View view = new View(model);
        Controller events_controller = new Controller(model, view);

        model.initObservers(view.getObserver());
        
        view.init(events_controller);
    }
    
}
