package Controller;

import Model.Model;
import View.View;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;

/**
 * Clase que controla la interacció del controlador amb els altres mòduls
 * @author bernat
 */
public class Controller implements ActionListener {

    private final Model model;
    private final View view;

    private Process c;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        c = new Process(model, view);
    }

    public static void Config(){}
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            String accio = ae.getActionCommand();
            switch (accio) {
                case "compute":
                    Thread t = new Thread(c);
                    t.start();
                    break;
                case "selectFile":
                    int returnVal = view.fileChooser().showOpenDialog(view.getFrame());

                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = view.fileChooser().getSelectedFile();
                        model.setSelectedFile(file);
                        //This is where a real application would open the file.
                        System.out.println("Opening: " + file.getName() + ".");
                    } else {
                        System.out.println("Open command cancelled by user.");
                    }
                    break;
                default:
                    throw new badSelection();
            }

        } catch (badSelection ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private class badSelection extends Exception {
    }

}
