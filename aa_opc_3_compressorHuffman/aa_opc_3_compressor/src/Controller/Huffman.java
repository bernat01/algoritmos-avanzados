/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bernat
 */
public class Huffman {
    private File file;
    private HashMap<Integer, Symbol> Huffman;

    public Huffman(File file) {
        this.file = file;
    }
    
    /**
     * Executa el procés de compressió de Huffman
     * @return boolean indica si el procés ha anat bé
     */
    public boolean getCompressFile(){
        //build the leaves nodes
        PriorityQueue<Node> nodes = buildPriorityQueue();
        InternalNode newNode = null;
        
        //build the tree
        while(nodes.size() > 1){
            Node n1 = nodes.poll();//retrieve and remove
            Node n2 = nodes.poll();//retrieve and remove
            newNode = new InternalNode(n1, n2, n1.getFreq()+n2.getFreq());
            nodes.offer(newNode);
        }
        
        //build the huffmanTable
        Node root = nodes.poll();
        buildHuffmanTable(root, 0, 0);
        return writeOutputFile();       
    }
    
     /**
     * Recorr el fitxer a comprimir i crea els nodes de Symbols, calculant el seu
     * nombre d'aparicions
     * @return coa de prioritat amb tots els nodes creats ordenats per frequencia
     */
    private PriorityQueue<Node> buildPriorityQueue(){
        try {
            this.Huffman = new HashMap<Integer, Symbol>();
            FileInputStream file = new FileInputStream(this.file);
            Integer data;
            while((data= file.read()) != -1){
                Symbol code = this.Huffman.get(data);
                if(code == null){
                    code = new Symbol(data);
                    this.Huffman.put(data, code);
                }
                code.incFreq();          
            }
            return new PriorityQueue<Node>(this.Huffman.values());
        } catch (IOException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
      
    /**
     * Recorregut de l'àrbre, per assignar a cada symbol el seu codi de Huffman
     * @param node Node a comprovar de la crida
     * @param code codi a assignar si el node es d'un simbol
     * @param level profunditat de l'arbre actual
     */
    private void buildHuffmanTable(Node node, int code, int level){
        if (node instanceof Symbol){
            Symbol nodeS = (Symbol) node;
            nodeS.setCode(code);
            
        }else{
            Node lc = ((InternalNode)node).getLeftChild();
            Node rc = ((InternalNode)node).getRightChild();
            //put the bit num level to 0 -> left child, 1 -> right child
            int mask = 1 << level ;
            int lCode = code & ~mask;
            int rCode = code | mask;
           
            buildHuffmanTable(lc, lCode, level +1);
            buildHuffmanTable(rc, rCode, level +1);
        }        
    }
    
    /**
     * Escriu el fitxer comprimit
     * @return indica si l'execució ha estat satisfactòria
     */
    private boolean writeOutputFile(){
        FileInputStream in = null;
        try { 
            Integer data;
            in = new FileInputStream(this.file);
            FileOutputStream out = new FileOutputStream(this.getOutputFilePath());
            String output ="";
            while((data= in.read()) != -1){
                Symbol symbol = this.Huffman.get(data);
                if(symbol == null){//per seguretat
                    throw new Exception("bad huffman table");
                }
                output += Integer.toBinaryString(symbol.getCode());
                while(output.length() >= 8){
                    String byteStr = output.substring(0, 8);
                    byte byt = Utils.stringToByte(byteStr);
                    out.write(byt);
                    output = output.substring(8, output.length());
                }
            }
            if(output.length() > 0){//del bucle sempre sortim amb un string de entre  0 i 8 digits
                String byteStr = output.substring(0, output.length());
                while(byteStr.length() < 8){
                    byteStr+="0";
                }
                byte byt = Utils.stringToByte(byteStr);
                out.write(byt);
            }
            in.close();
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Huffman.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(Huffman.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
                
    public String getOutputFilePath(){
        return this.file.getPath() + ".compress";
    }
    
    public void debug(){
        for (HashMap.Entry<Integer, Symbol> entry : this.Huffman.entrySet()) {
            Integer key = entry.getKey();
            Symbol value = entry.getValue();
            System.out.println("value: " + Integer.toBinaryString(key) + " code: " + Integer.toBinaryString(value.getCode()));
        }
    }
   
    private class Node implements Comparable{
        protected int freq;
           
        public void incFreq(){
            this.freq++;
        }   

        public int getFreq() {
            return freq;
        }
        
        
        @Override
        public int compareTo(Object t) {
            return ((Integer)this.getFreq()).compareTo((Integer) ((Node)t).getFreq());
        }
    }
    
    private class InternalNode extends Node{
        private Node child1;
        private Node child2;

        public InternalNode(Node child1, Node child2, int freq) {
            this.child1 = child1;
            this.child2 = child2;
            this.freq = freq;
        }

        public Node getLeftChild() {
            return child1;
        }

        public Node getRightChild() {
            return child2;
        }
        @Override
        public String toString() {
            return "Internal --> freq: "+freq;
        }

    }
    
    private class Symbol extends Node{
        private int value;
        private int code;
        

        public Symbol(int code) {
            this.value = code;
            this.freq = 0;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
        
        @Override
        public String toString() {
            return "value: " + value + " freq: "+freq + " HuffCode: "+Integer.toBinaryString(code);
        }
    }
}
