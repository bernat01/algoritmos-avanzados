package Controller;

import Model.Message;
import Model.Model;
import View.View;
import java.io.File;

public class Process implements Runnable {
    private Model model;
    private View view;

    public Process(Model model, View view) {
        this.model = model;
        this.view = view;
    }

  

    @Override
    public void run() {
        File f = model.getSelectedFile();
        if (f == null) {
            //show message dialog
            view.alert(Message.NO_FILE);
            return;
        }
        Huffman huf = new Huffman(f);
        if(huf.getCompressFile()){
            view.alert(Message.PROCESS_SUCCESS+huf.getOutputFilePath());
            huf.debug();
        }else{
            view.alert(Message.PROCESS_FAIL);
        }

    }

}
