package Controller;

/**
 * Classe que engloba les funcions estatiques independents de cap clase del controlador
 * @author bernat
 */
public class Utils {
    
    public static byte stringToByte(String str_byte) {
        int out = 0;
        for (int i = 0; i < str_byte.length(); i++) {
            char cbit = str_byte.charAt(i);
            out = out << 1;
            if (cbit == '1') {
                out = out | 1;

            }
        }
        return (byte) out;
    }
}
