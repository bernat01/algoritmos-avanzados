package Model;

/**
 *
 * @author bernat
 */
public class Message {
   public final static String NO_FILE =  "No has seleccionado ningún fichero";
   public final static String PROCESS_FAIL = "Error comprimiendo el fichero";
   public final static String PROCESS_SUCCESS = "Fichero comprimido con exito. Ruta: ";
}
