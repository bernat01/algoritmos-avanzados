package Model;

import java.io.File;
import java.util.Observable;

/**
 *
 * @author berna
 */
public class Model extends Observable  {
    
    //window data
    private static int w = 350;
    private static int h = 75;
    
    private final String WIN_NAME = "Compressor Huffman"; 
    
    //data
    private File selectedFile = null;
    
    public Model() {}
   
    public static int getW() {
        return w;
    }

    public static int getH() {
        return h;
    }

    public String getWIN_NAME() {
        return WIN_NAME;
    }

    public void setSelectedFile(File selectedFile) {
        this.selectedFile = selectedFile;
        
        this.setChanged();
        this.notifyObservers();
    }

    public File getSelectedFile() {
        return selectedFile;
    }
    
    public String getSelectedFilePath(){
        if (this.selectedFile == null){
            return "";
        }
        return this.selectedFile.getPath();
    }    

}
