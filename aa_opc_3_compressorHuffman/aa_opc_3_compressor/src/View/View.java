/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Model;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author berna
 */
public class View implements Observer{
    
    private Model model;
    private Window gui;
    
    public View(Model model) {
        this.model = model;        
        gui = new Window(this.model.getWIN_NAME());
        gui.setVisible(true);
    }
    
    public static void Config(){
        Window.setDimensions(Model.getW(), Model.getH());
    }
    
    public void addController(ActionListener controller) {
        gui.getBtn_comprime().setActionCommand("compute");
        gui.getBtn_comprime().addActionListener(controller);
        
        gui.getBtn_selectFile().setActionCommand("selectFile");
        gui.getBtn_selectFile().addActionListener(controller);
     }
     
    public void mostrar() {
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.pack();
        gui.setVisible(true);
    }
    
    @Override
    public void update(Observable o, Object arg) {
        gui.setTextLabelSelFile(this.model.getSelectedFilePath());
    }
    
    public JFileChooser fileChooser(){
        return this.gui.getFc();
    }
    
    public JFrame getFrame(){
        return this.gui;
    }
    
     
    public void alert(String text){
        JOptionPane.showMessageDialog(null, text);
    }
    
    /** Returns an ImageIcon, or null if the path was invalid. */
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = View.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
