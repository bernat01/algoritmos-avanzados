package aa_opc_3_compressor;

import core.Core;

/**
 *Efectuar una aplicación siguiendo el patrón de diseño MVC. 
 * Para poder efectuar correctamente el patrón de diseño MVC de nuevo deberemos 
 * disponer de un lenguage que permita la POO o que disponga del uso de TAD.
 * El programa deberá de presentar una interfaz que pueda recibir un archivo de entrada, 
 * analizarlo, asociarle el código de Huffman correspondiente a la técnica ávida 
 * vista en clase y generar un archivo comprimido de salida.
 * @author bernat
 */
public class Aa_opc_3_compressor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Core.init();
    }
    
}
