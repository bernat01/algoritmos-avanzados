/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import Controller.Controller;
import Model.Model;
import View.View;

/**
 *
 * @author bernat
 */
public class Core {
    public static void init(){
        /**
         * Configuram contingut estatic provenent del model
         */
        Controller.Config();
        View.Config();
        
        /**
         * Inici sistema
         */
        Model model = new Model();
        View view = new View(model);
        Controller controller= new Controller(model, view);
        
        view.addController(controller);
        model.addObserver(view);
        
        view.mostrar();
    }
}
